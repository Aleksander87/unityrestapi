using System;
using UnityEditor;

namespace Editor
{
    public class GenericPopupEditorWindow : EditorWindow
    {
        public Action onGuiCallback;
        
        private void OnGUI()
        {
            onGuiCallback?.Invoke();
        }
    }
}