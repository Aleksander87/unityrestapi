using System;
using System.Collections.Generic;
using System.Linq;
using Authentication;
using JsonEditorUtility;
using Requests.VirtualGoods.Data;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class ProjectWindow : EditorWindow
    {
        [MenuItem("WhatSparks/Project")]
        private static void ShowWindow()
        {
            var window = GetWindow<ProjectWindow>();
            window.titleContent = new GUIContent("Project");
            window.minSize = new Vector2(400, 40);
            window.Show();
        }

        private List<Game> games;
        private Game currentGame;
        private string newGameName, newGameApiKey;
        private bool adding;
        private string[] gamesName;
        private int gameIndex, ambientIndex;

        private void OnEnable()
        {
            UpdateGameList();
        }

        private void OnFocus() => UpdateGameList();

        private void UpdateGameList()
        {
            games = ProjectUtility.GetGames();
            gamesName = (from game in games select game.Name).ToArray();

            if (games.Any())
                currentGame = games[Mathf.Clamp(gameIndex, 0, games.Count - 1)];

            if (currentGame == null) 
                return;
            
            ProjectUtility.GsApiKey = currentGame.ApiKey;
            ProjectUtility.Ambient = (ProjectUtility.GsAmbient) ambientIndex;
        }

        private void OnGUI()
        {
            if (!games.Any())
            {
                if(GUILayout.Button("Add Game"))
                    CreateGamePopup();

                return;
            }
            
            if(currentGame == null)
                UpdateGameList();
            
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();

            EditorGUI.BeginChangeCheck();
            
            gameIndex = EditorGUILayout.Popup("Game", gameIndex, gamesName);
            ambientIndex = EditorGUILayout.Popup("Ambient", ambientIndex, Enum.GetNames(typeof(ProjectUtility.GsAmbient)));

            if (EditorGUI.EndChangeCheck())
            {
                AuthenticationUtility.Logout();
                UpdateGameList();
            }

            EditorGUILayout.EndVertical();
            
            GUI.enabled = false;
            EditorGUILayout.TextField("Api Key", currentGame.ApiKey);
            GUI.enabled = true;

            EditorGUILayout.BeginVertical();

            GUI.backgroundColor = Color.red;
            if (GUILayout.Button("X", GUILayout.Width(20)))
            {
                ProjectUtility.RemoveGame(currentGame.Name);
                UpdateGameList();
            }
            GUI.backgroundColor = Color.white;

            if (GUILayout.Button("+", GUILayout.Width(20)))
                CreateGamePopup();
            
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }

        private void CreateGamePopup()
        {
            var popup = GetWindow<GenericPopupEditorWindow>();
            popup.position = new Rect(
                position.position.x + position.size.x / 2 - 200, 
                position.position.y + position.size.y / 2 - 75, 400, 150);
            popup.titleContent = new GUIContent("Add Game");
            popup.onGuiCallback = () => AddPopupCallback(popup);

            newGameName = "";
            newGameApiKey = "";
            
            popup.ShowPopup();
        }

        private void AddPopupCallback(EditorWindow popup)
        {
            GUILayout.Space(20);
            newGameName = EditorGUILayout.TextField("New Game Name", newGameName);
            newGameApiKey = EditorGUILayout.TextField("New Game Api Key", newGameApiKey);
            GUILayout.Space(20);

            EditorGUILayout.BeginHorizontal();

            GUI.enabled = !string.IsNullOrEmpty(newGameName) &&
                          !string.IsNullOrEmpty(newGameApiKey) &&
                          games.All(g => g.Name != newGameName && g.ApiKey != newGameApiKey);

            if (GUILayout.Button("Confirm"))
            {
                ProjectUtility.AddGame(newGameName, newGameApiKey);
                popup.Close();
            }

            GUI.enabled = true;

            if(GUILayout.Button("Cancel"))
                popup.Close();
                    
            EditorGUILayout.EndHorizontal();
        }
    }
}