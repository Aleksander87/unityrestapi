using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace JsonEditorUtility
{
    /// <summary>Draw the editor of a json string.</summary>
    public class JsonDrawer
    {
        private readonly Parser _parser;
        public string Json { get; private set; }

        public JsonDrawer(string json)
        {
            Json = json;
            _parser = new Parser();
        }

        public string Draw(bool sealedData = false)
        {
            return Json = _parser.Draw(Json, sealedData);
        }

        private class Parser : IDisposable
        {
            private StringReader _stringReader;
            private readonly GUIStyle _typeStyle;

            private bool _firstObject, _deleted, _sealedData;
            private int _controlNumber, _toChangeNumber, _toEditNumber, _toDeleteNumber;
            private string _changeString, _insertString;
            private Rect _currentRect;
            private Dictionary<int, AnimBool> _foldoutMap;
            private Stack<int> _currentFoldoutIndex;

            private const string ColorTextFormat = "<color=#FFFFFF66>{0}</color>";

            public Parser()
            {
                _typeStyle = new GUIStyle
                {
                    richText = true,
                    alignment = TextAnchor.MiddleRight,
                    fontStyle = FontStyle.Italic,
                    fontSize = 13
                };
                
                _foldoutMap = new Dictionary<int, AnimBool>();
                _currentFoldoutIndex = new Stack<int>();
                
                _toChangeNumber = -1;
                _toDeleteNumber = -1;
            }

            public string Draw(string jsonString, bool sealedData)
            {
                _controlNumber = 0;
                _sealedData = sealedData;

                if (_deleted)
                {
                    _toDeleteNumber = -1;
                    _deleted = false;
                }

                _firstObject = true;

                using (_stringReader = new StringReader(jsonString))
                {
                    EditorGUI.indentLevel++;
                    var result = ParseValue();
                    EditorGUI.indentLevel--;
                    return result;
                }
            }

            public void Dispose()
            {
                _stringReader.Dispose();
                _stringReader = null;
            }

            #region Foldouts
            
            private void BeginFadeGroup(int number)
            {
                if(!_foldoutMap.ContainsKey(number))
                    _foldoutMap.Add(number, new AnimBool(false));

                EditorGUI.indentLevel -= 2;

                var arrowRect = new Rect(new Vector2(3, _currentRect.y), new Vector2(15, _currentRect.height));
                EditorGUI.Foldout(arrowRect, _foldoutMap[number].target, "");
                
                EditorGUI.indentLevel += 2;
                
                EditorGUILayout.BeginFadeGroup(_foldoutMap[number].faded);

                _currentFoldoutIndex.Push(number);
            }
            
            private void EndFadeGroup()
            {
                EditorGUILayout.EndFadeGroup();
                _currentFoldoutIndex.Pop();
            }

            private void SwitchFadeGroup(int number)
            {
                _foldoutMap[number].target = !_foldoutMap[number].target;
                Event.current.Use();
            }

            private void AddedControls(int afterControlNumber, int quantity)
            {
                var temp = new Dictionary<int, AnimBool>();

                foreach (var f in _foldoutMap)
                    if (f.Key <= afterControlNumber)
                        temp.Add(f.Key, f.Value);
                    else
                        temp.Add(f.Key + quantity, f.Value);

                _foldoutMap = temp;
                
                var temp2 = new Stack<int>();
                
                foreach (var f in _currentFoldoutIndex)
                    if (f <= afterControlNumber)
                        temp2.Push(f);
                    else
                        temp2.Push(f + quantity);

                _currentFoldoutIndex = temp2;
            }
            
            #endregion

            private void DrawMenuItem(Event currentEvent)
            {
                var currentControl = _controlNumber;
                var menu = new GenericMenu();
                
                menu.AddItem(new GUIContent("Edit Field/Delete"), false, () =>
                {
                    _toDeleteNumber = currentControl;
                    AddedControls(currentControl, 1);
                });
                menu.AddSeparator("");
                menu.AddItem(new GUIContent("Type Change/String"), false, () =>
                {
                    _changeString = "\"\"";
                    _toChangeNumber = currentControl;
                });
                menu.AddItem(new GUIContent("Type Change/Number"), false, () =>
                {
                    _changeString = "0";
                    _toChangeNumber = currentControl;
                });
                menu.AddItem(new GUIContent("Type Change/Boolean"), false, () =>
                {
                    _changeString = "false";
                    _toChangeNumber = currentControl;
                });
                menu.AddItem(new GUIContent("Type Change/Object"), false, () =>
                {
                    _changeString = "{}";
                    _toChangeNumber = currentControl;
                });
                menu.AddItem(new GUIContent("Type Change/Array/String"), false, () =>
                {
                    _changeString = "[\"\"]";
                    _toChangeNumber = currentControl;
                });
                menu.AddItem(new GUIContent("Type Change/Array/Number"), false, () =>
                {
                    _changeString = "[0]";
                    _toChangeNumber = currentControl;
                });
                menu.AddItem(new GUIContent("Type Change/Array/Object"), false, () =>
                {
                    _changeString = "[{}]";
                    _toChangeNumber = currentControl;
                });
                menu.AddItem(new GUIContent("Type Change/Array/Boolean"), false, () =>
                {
                    _changeString = "[false]";
                    _toChangeNumber = currentControl;
                });
                menu.AddItem(new GUIContent("Type Change/Array/Array"), false, () =>
                {
                    _changeString = "[[]]";
                    _toChangeNumber = currentControl;
                });
                
                menu.ShowAsContext();
                currentEvent.Use();
            }

            private string ReturnCheck(string value, int checkControl = -1, bool isFieldName = false)
            {
                var toCheck = checkControl > -1 ? checkControl : _controlNumber;

                if (Event.current.type != EventType.Repaint) 
                    return value;
                
                if (!isFieldName && toCheck == _toChangeNumber)
                {
                    _toChangeNumber = -1;
                    return _changeString;
                }

                if (toCheck == _toDeleteNumber)
                {
                    _deleted = true;
                    return "";
                }

                return value;
            }

            private string ParseObject()
            {
                var firstObject = _firstObject;
                _firstObject = false;
                var addField = false;
                var myControlNumber = _controlNumber;
                
                if (!firstObject)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.LabelField(string.Format(ColorTextFormat, "object"), _typeStyle, GUILayout.Width(50));

                    if (!_sealedData && GUILayout.Button("+", GUILayout.Width(25)))
                        addField = true;
                    
                    var ce = Event.current;
                    
                    if(_currentRect.Contains(ce.mousePosition) && ce.type == EventType.MouseUp)
                        SwitchFadeGroup(myControlNumber);

                    EditorGUILayout.EndHorizontal();
                    
                    BeginFadeGroup(myControlNumber);
                }
                
                var builder = new StringBuilder();
                builder.Append('{');
                _stringReader.Read();

                while (true)
                {
                    TOKEN nextToken;
                    do
                    {
                        nextToken = NextToken;
                        
                        switch (nextToken)
                        {
                            case TOKEN.NONE:
                                
                                if(!firstObject)
                                    EditorGUI.indentLevel--;
                                
                                return ReturnCheck("");
                            case TOKEN.CURLY_OPEN:
                                builder.Append('{');
                                goto label_5;
                            case TOKEN.CURLY_CLOSE:

                                if (!firstObject)
                                {
                                    if (!_sealedData && addField)
                                    {
                                        builder.Append("\"new string\":\"\"");
                                        AddedControls(myControlNumber, 1);
                                    }
                                    
                                    EditorGUI.indentLevel--;
                                    
                                    builder.Append('}');
                                    
                                    EndFadeGroup();
                                }
                                else
                                {
                                    EditorGUILayout.BeginHorizontal();
                                    
                                    if(!_sealedData && GUILayout.Button("+"))
                                        builder.Append("\"new string\":\"\"");
                                    
                                    builder.Append('}');

                                    if (GUILayout.Button("To Text File"))
                                    {
                                        var path = Application.dataPath + "/document.txt";
                                        File.WriteAllText(path, builder.ToString());
                                        System.Diagnostics.Process.Start(path);
                                    }
                                    
                                    EditorGUILayout.EndHorizontal();
                                }

                                return ReturnCheck(builder.ToString());
                            case TOKEN.COMMA:
                                builder.Append(ReturnCheck(","));
                                continue;
                            default:
                                continue;
                        }
                    } while (nextToken == TOKEN.COMMA);

                    
                    label_5:
                    
                    _currentRect = EditorGUILayout.BeginHorizontal();
                    _controlNumber++;

                    var currentEvent = Event.current;

                    if (!_sealedData && 
                        _currentRect.Contains(currentEvent.mousePosition) &&
                        currentEvent.type == EventType.ContextClick)
                    {
                        DrawMenuItem(currentEvent);
                    }
                    
                    var index = ParseString(true);

                    builder.Append(ReturnCheck(index, -1, true));
                    
                    if (index != null)
                    {
                        if (NextToken == TOKEN.COLON)
                        {
                            _stringReader.Read();
                            var currentControl = _controlNumber;
                            var result = ReturnCheck($":{ParseValue()}", currentControl);
                            builder.Append(result);
                        }
                        else
                        {
                            if(!firstObject)
                                EditorGUI.indentLevel--;
                            
                            return ReturnCheck("");
                        }
                    }
                    else
                    {
                        if(!firstObject)
                            EditorGUI.indentLevel--;
                        
                        return ReturnCheck("");
                    }
                }
            }

            private string ParseArray()
            {
                _stringReader.Read();
                var flag = true;
                var firstElement = true;
                var myControlNumber = _controlNumber;
                var added = false;
                var removed = false;
                var sizeRect = new Rect(_currentRect.position, 
                    new Vector2(_currentRect.width - 50, _currentRect.height));
                
                EditorGUILayout.LabelField(string.Format(ColorTextFormat, "array"), _typeStyle, GUILayout.Width(50));
                
                if (GUILayout.Button("+", GUILayout.Width(25)))
                    added = true;
                if (GUILayout.Button("-", GUILayout.Width(25)))
                    removed = true;
                
                EditorGUI.indentLevel++;

                var builder = new StringBuilder();
                var elements = new List<string>();
                
                while (flag)
                {
                    var nextToken = NextToken;
                    switch (nextToken)
                    {
                        case TOKEN.NONE:
                            break;
                        case TOKEN.SQUARED_CLOSE:
                            flag = false;
                            continue;
                        case TOKEN.COMMA:
                            continue;
                        default:

                            if (firstElement)
                            {
                                var ce = Event.current;

                                if (_currentRect.Contains(ce.mousePosition) &&
                                    ce.type == EventType.MouseUp)
                                {
                                    SwitchFadeGroup(myControlNumber);
                                }
                                
                                EditorGUILayout.EndHorizontal();
                                firstElement = false;
                                
                                BeginFadeGroup(myControlNumber);
                            }

                            _currentRect = EditorGUILayout.BeginHorizontal();
                            _controlNumber++;

                            //Disabled modification of array element type
                            
                            // var currentEvent = Event.current;
                            //
                            // if (!_sealedData && 
                            //     _currentRect.Contains(currentEvent.mousePosition) &&
                            //     currentEvent.type == EventType.ContextClick)
                            // {
                            //     DrawMenuItem(currentEvent);
                            // }
                            
                            EditorGUILayout.LabelField($"Element {elements.Count}", GUILayout.Width(EditorGUIUtility.labelWidth));
                            elements.Add(ReturnCheck(ParseByToken(nextToken)));
                            
                            continue;
                    }
                }

                //Any elements ?
                if (!firstElement)
                    EndFadeGroup();
                else
                    EditorGUILayout.EndHorizontal();

                if (added)
                {
                    AddedControls(myControlNumber, 1);
                    elements.Add(elements.Any() ? elements.Last() : "\"\"");
                }

                if (removed && elements.Any())
                {
                    AddedControls(myControlNumber, -1);
                    elements.RemoveAt(elements.Count -1);
                }
                
                EditorGUI.LabelField(sizeRect, string.Format(ColorTextFormat, $"Size: {elements.Count}"), _typeStyle);

                builder.Append('[');

                for (int i = 0; i < elements.Count; i++)
                {
                    builder.Append(elements[i]);

                    if (i < elements.Count - 1)
                        builder.Append(',');
                }
                
                builder.Append(']');

                EditorGUI.indentLevel--;

                return ReturnCheck(builder.ToString());
            }

            private string ParseValue()
            {
                return ParseByToken(NextToken);
            }

            private string ParseByToken(TOKEN token)
            {
                switch (token)
                {
                    case TOKEN.CURLY_OPEN:
                        return ParseObject();
                    case TOKEN.SQUARED_OPEN:
                        return ParseArray();
                    case TOKEN.STRING:
                        return ParseString(false);
                    case TOKEN.NUMBER:
                        return ParseNumber();
                    case TOKEN.TRUE:
                        return ParseBoolean(true);
                    case TOKEN.FALSE:
                        return ParseBoolean(false);
                    case TOKEN.NULL:
                        return "null";
                    default:
                        return "null";
                }
            }

            private string ParseBoolean(bool value)
            {
                EditorGUILayout.LabelField(string.Format(ColorTextFormat, "bool"), _typeStyle, GUILayout.Width(50));
                value = EditorGUILayout.Toggle(value);
                EditorGUILayout.EndHorizontal();
                return ReturnCheck(value.ToString().ToLower());
            }

            private string ParseString(bool isFieldName)
            {
                var stringBuilder1 = new StringBuilder();
                _stringReader.Read();
                var flag = true;
                while (flag)
                {
                    if (_stringReader.Peek() == -1)
                        break;
                    var nextChar1 = NextChar;
                    switch (nextChar1)
                    {
                        case '"':
                            flag = false;
                            continue;
                        case '\\':
                            if (_stringReader.Peek() == -1)
                            {
                                flag = false;
                                continue;
                            }

                            char nextChar2 = NextChar;
                            switch (nextChar2)
                            {
                                case '"':
                                case '/':
                                case '\\':
                                    stringBuilder1.Append(nextChar2);
                                    continue;
                                case 'b':
                                    stringBuilder1.Append('\b');
                                    continue;
                                case 'f':
                                    stringBuilder1.Append('\f');
                                    continue;
                                case 'n':
                                    stringBuilder1.Append('\n');
                                    continue;
                                case 'r':
                                    stringBuilder1.Append('\r');
                                    continue;
                                case 't':
                                    stringBuilder1.Append('\t');
                                    continue;
                                case 'u':
                                    StringBuilder stringBuilder2 = new StringBuilder();
                                    for (int index = 0; index < 4; ++index)
                                        stringBuilder2.Append(NextChar);
                                    stringBuilder1.Append(
                                        (char) Convert.ToInt32(stringBuilder2.ToString(), 16));
                                    continue;
                                default:
                                    continue;
                            }
                        default:
                            stringBuilder1.Append(nextChar1);
                            continue;
                    }
                }

                var result = stringBuilder1.ToString();

                if (isFieldName)
                {
                    if (_currentRect.Contains(Event.current.mousePosition))
                        _toEditNumber = _controlNumber;

                    if (!_sealedData && _toEditNumber == _controlNumber)
                        result = EditorGUILayout.TextField(result, GUILayout.Width(EditorGUIUtility.labelWidth));
                    else
                        EditorGUILayout.LabelField(result, GUILayout.Width(EditorGUIUtility.labelWidth));
                }
                else
                {
                    EditorGUILayout.LabelField(string.Format(ColorTextFormat, "string"),_typeStyle, GUILayout.Width(50));
                    result = EditorGUILayout.TextField(result);
                    EditorGUILayout.EndHorizontal();
                }

                result = $"\"{result}\"";

                return ReturnCheck(result, -1, isFieldName);
            }

            private string ParseNumber()
            {
                EditorGUILayout.LabelField(string.Format(ColorTextFormat, "number"),_typeStyle, GUILayout.Width(50));
                double.TryParse(NextWord, NumberStyles.Any, CultureInfo.InvariantCulture,
                    out var result);
                result = EditorGUILayout.DoubleField(result);
                EditorGUILayout.EndHorizontal();
                return ReturnCheck(result.ToString(CultureInfo.InvariantCulture));
            }

            private void EatWhitespace()
            {
                while (" \t\n\r".IndexOf(PeekChar) != -1)
                {
                    _stringReader.Read();
                    if (_stringReader.Peek() == -1)
                        break;
                }
            }

            private char PeekChar => Convert.ToChar(_stringReader.Peek());

            private char NextChar => Convert.ToChar(_stringReader.Read());

            private string NextWord
            {
                get
                {
                    var stringBuilder = new StringBuilder();
                    while (" \t\n\r{}[],:\"".IndexOf(PeekChar) == -1)
                    {
                        stringBuilder.Append(NextChar);
                        if (_stringReader.Peek() == -1)
                            break;
                    }

                    return stringBuilder.ToString();
                }
            }

            private TOKEN NextToken
            {
                get
                {
                    EatWhitespace();
                    if (_stringReader.Peek() == -1)
                        return TOKEN.NONE;
                    
                    switch (PeekChar)
                    {
                        case '"':
                            return TOKEN.STRING;
                        case ',':
                            _stringReader.Read();
                            return TOKEN.COMMA;
                        case '-':
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                        case 'I':
                        case 'N':
                            return TOKEN.NUMBER;
                        case ':':
                            return TOKEN.COLON;
                        case '[':
                            return TOKEN.SQUARED_OPEN;
                        case ']':
                            _stringReader.Read();
                            return TOKEN.SQUARED_CLOSE;
                        case '{':
                            return TOKEN.CURLY_OPEN;
                        case '}':
                            _stringReader.Read();
                            return TOKEN.CURLY_CLOSE;
                        default:
                            switch (NextWord)
                            {
                                case "false":
                                    return TOKEN.FALSE;
                                case "true":
                                    return TOKEN.TRUE;
                                case "null":
                                    return TOKEN.NULL;
                                default:
                                    return TOKEN.NONE;
                            }
                    }
                }
            }

            private enum TOKEN
            {
                NONE,
                CURLY_OPEN,
                CURLY_CLOSE,
                SQUARED_OPEN,
                SQUARED_CLOSE,
                COLON,
                COMMA,
                STRING,
                NUMBER,
                TRUE,
                FALSE,
                NULL,
            }
        }
    }
}