using System;
using Authentication;
using UnityEditor;
using UnityEngine;
using System.IO;
using UnityEditor.AnimatedValues;

namespace Editor
{
    public class LoginEditorWindow : EditorWindow
    {
        [MenuItem("WhatSparks/Login")]
        private static void ShowWindow()
        {
            var window = GetWindow<LoginEditorWindow>();
            window.titleContent = new GUIContent("User");
            window.minSize = new Vector2(400, 40);
            window.Show();
        }

        private string _username, _password;
        private string _rememberMeKey;
        private bool _rememberMe;
        private AnimBool _fadeBool;
        private GUIStyle _loadingStyle;

        private void OnEnable()
        {
            _rememberMe = File.Exists(AuthenticationUtility.UserBasicAddress);
            
            if(_rememberMe)
                AuthenticationUtility.AutoLogin();
            
            _fadeBool = new AnimBool(AuthenticationUtility.LoggedSate == LoginState.LogInProgress);

            _loadingStyle = new GUIStyle
            {
                richText = true,
                fontSize = 18,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleCenter
            };
        }

        private void Update() => Repaint();

        private void OnGUI()
        {
            switch (AuthenticationUtility.LoggedSate)
            {
                case LoginState.NotLogged:
                    DrawNotLogged();
                    break;
                case LoginState.Logged:
                    DrawLogged();
                    break;
                case LoginState.LogInProgress:
                    DrawLogging();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void DrawLogged()
        {
            EditorGUILayout.LabelField("Logged as", AuthenticationUtility.UserName);

            if(GUILayout.Button("Logout", GUILayout.Height(35)))
                AuthenticationUtility.Logout();
        }

        private void DrawNotLogged()
        {
            _username = EditorGUILayout.TextField("Username", _username);
            _password = EditorGUILayout.PasswordField("Password", _password);
                
            EditorGUILayout.BeginHorizontal();

            EditorGUI.BeginChangeCheck();
            _rememberMe = EditorGUILayout.Toggle("Remember Me", _rememberMe, GUILayout.Width(160));
            if (EditorGUI.EndChangeCheck())
                if(!_rememberMe && File.Exists(AuthenticationUtility.UserBasicAddress))
                    File.Delete(AuthenticationUtility.UserBasicAddress);
                
            GUI.enabled = !string.IsNullOrEmpty(_username) &&
                          !string.IsNullOrEmpty(_password);
                
            if(GUILayout.Button("Login", GUILayout.Width(160)))
                AuthenticationUtility.Login(_username, _password, _rememberMe);
        }
        
        private void DrawLogging()
        {
            var loadWriting = "Logging";

            var pointCount = Mathf.RoundToInt((float) (EditorApplication.timeSinceStartup % 4));

            for (var i = 0; i < pointCount; i++)
                loadWriting += ".";

            DrawOverlayWriting(loadWriting, 1 - _fadeBool.faded);
        }
        
        private void DrawOverlayWriting(string message, float fade = 1)
        {
            message = $"<color=white>{message}</color>";
            
            EditorGUILayout.LabelField(message, _loadingStyle, GUILayout.Height(25));
            
            if(GUILayout.Button("Cancel", GUILayout.Height(25)))
                AuthenticationUtility.CancelLogin();
        }
    }
}