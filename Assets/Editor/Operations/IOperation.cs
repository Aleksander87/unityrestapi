using Requests.Base;

namespace Editor.Operations
{
    public interface IOperation
    {
        string Description { get; }
        string Name { get; }
        bool Initialized { get; }
        RequestState State { get; }
        void DrawEditorGUI();
        void Initialize();
        void Reload();
    }
}