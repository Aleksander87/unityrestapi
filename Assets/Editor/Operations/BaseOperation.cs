using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Requests.Base;
using UnityEditor;
using UnityEngine;

namespace Editor.Operations
{
    public abstract class BaseOperation : IOperation
    {
        private List<RestRequest> _restRequests;
        private string _searchKey;
        private bool _aborted, _updatingProgressBar;
        
        public abstract string Description { get; }
        public abstract string Name { get; }
        public bool Initialized => _restRequests != null;
        
        public virtual RequestState State
        {
            get
            {
                if (_updatingProgressBar)
                    return RequestState.InProgress;
                
                if (_restRequests != null)
                    foreach (var request in _restRequests)
                        if((request?.State ?? RequestState.Idle) == RequestState.InProgress)
                            return RequestState.InProgress;
                
                return RequestState.Idle;
            }
        }
        
        public abstract void Initialize();
        public virtual void Reload() => Initialize();

        protected void AddRequests(bool displayProgressBar, Action onConcluded = null, params RestRequest[] requests)
        {
            _restRequests = requests.ToList();

            if (!displayProgressBar)
                return;

            UpdateAll(onConcluded);
        }

        protected void DrawSearchBar(ref int index, IList<string> names, Action onChanged = null, bool expand = true)
        {
            GUILayout.Space(10);
            
            EditorGUI.BeginChangeCheck();
            
            _searchKey = expand 
                ? EditorGUILayout.TextField("Search Key", _searchKey, GUILayout.Width(500)) 
                : EditorGUILayout.TextField("Search Key", _searchKey);
            
            GUILayout.Space(10);

            if (!EditorGUI.EndChangeCheck())
                return;

            var r = names.FirstOrDefault(n => n.ToLower().StartsWith(_searchKey.ToLower()));

            if (string.IsNullOrEmpty(r))
                r = names.FirstOrDefault(n => n.ToLower().Contains(_searchKey.ToLower()));
            
            var newIndex = !string.IsNullOrEmpty(r) ? names.IndexOf(r) : index;
            
            if(newIndex != index)
                onChanged?.Invoke();

            index = newIndex;
        }

        public virtual void DrawEditorGUI()
        {
            if (!_updatingProgressBar || _aborted)
            {
                EditorUtility.ClearProgressBar();
                return;
            }

            var count = _restRequests.Count;
            var concluded = _restRequests.Count(r => r.State == RequestState.Concluded);

            if (EditorUtility.DisplayCancelableProgressBar(
                "Updating Requests",
                $"Concluded: {concluded} of {count}",
                (float) concluded / count))
            {
                _aborted = true;
                _updatingProgressBar = false;
                EditorUtility.ClearProgressBar();
            }
        }
        
        protected void DrawNavigationBar(ref int index, ICollection documents, Action onChanged, bool expand = true)
        {
            EditorGUILayout.BeginHorizontal();
            
            if(expand)
            EditorGUILayout.LabelField("", GUILayout.ExpandWidth(true));

            var oldIndex = index;
                        
            if (GUILayout.Button("<<", GUILayout.Width(50)))
                index = 0;
                        
            if (GUILayout.Button("<", GUILayout.Width(50)))
            {
                index--;
            
                if (index < 0)
                    index = documents.Count - 1;
            }
            
            EditorGUI.BeginChangeCheck();
            index = EditorGUILayout.IntField(index, GUILayout.Width(25));
            if (EditorGUI.EndChangeCheck())
                index = Mathf.Clamp(index, 0, documents.Count - 1);
                        
            EditorGUILayout.LabelField($"/ {documents.Count - 1}", GUILayout.Width(35));
            
            if (GUILayout.Button(">", GUILayout.Width(50)))
            {
                index++;
            
                if (index > documents.Count - 1)
                    index = 0;
            }
                        
            if (GUILayout.Button(">>", GUILayout.Width(50)))
                index = documents.Count - 1;
                        
            if(expand)
                EditorGUILayout.LabelField("", GUILayout.ExpandWidth(true));
            
            EditorGUILayout.EndHorizontal();

            if (index != oldIndex)
                onChanged?.Invoke();
        }

        private async Task UpdateAll(Action onConcluded)
        {
            _updatingProgressBar = true;
            
            foreach (var request in _restRequests)
            {
                var time = EditorApplication.timeSinceStartup;
                
                request.Send();

                while (request.State != RequestState.Concluded)
                {
                    await Task.Delay(100);

                    if (_aborted)
                    {
                        request.Request.Abort();
                        goto Label_aborted;
                    }
                }
                
                if(EditorApplication.timeSinceStartup - time < 6.5f)
                    await Task.Delay(100);
            }
            
            Label_aborted:
            
            _restRequests = new List<RestRequest>();
            _updatingProgressBar = false;
            onConcluded?.Invoke();
        }
    }
}