using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Requests.Base;
using Requests.VirtualGoods;
using Requests.VirtualGoods.Data;
using UnityEditor;
using UnityEngine;

namespace Editor.Operations
{
    public class VirtualGoodsOperation : BaseOperation
    {
        private ListVgsResponse _response;
        private int _index;
        private List<bool> _foldouts = new List<bool>();
        private GUIStyle _title, _subTitle;
        private SerializableVirtualGood _vgCopied;
        private Vector2 _scrollPos;
        private static readonly string VgCachePath = $"{Application.dataPath}/Prefs/[VirtualGoodsOperation] VgCache";

        public override string Name => "VirtualGoodsOperation";
        public override string Description => "Remove existing vg\nAdd new vg\nModify vg and segments";

        public VirtualGoodsOperation()
        {
            _title = new GUIStyle
            {
                richText = true,
                fontSize = 17,
                fontStyle = FontStyle.Bold
            };

            _subTitle = new GUIStyle
            {
                richText = true,
                fontSize = 16
            };
        }
        
        public override void Initialize()
        {
            var request = new GetListVgsRequest();
            _response = null;
            request.Than(r => _response = r);
            request.Catch(Debug.LogException);
            request.Send();
            _index = 0;
            AddRequests(false, null, request);
        }
        
        public override void DrawEditorGUI()
        {
            if (_response == null)
                return;
            
            if (!_response.VirtualGoods?.Any() ?? true)
            {
                EditorGUILayout.HelpBox("No virtual goods found!", MessageType.Warning);
                return;
            }
            
            base.DrawEditorGUI();

            DrawHeaderButtons();

            var names = (from vg1 in _response.VirtualGoods select vg1.ShortCode).ToList();
            DrawSearchBar(ref _index, names, ResetFoldouts);

            DrawNavigationBar(ref _index, _response.VirtualGoods, () =>
            {
                GUI.FocusControl(null);
                ResetFoldouts();
            });

            _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);

            GUILayout.Space(10);
            EditorGUILayout.LabelField("<color=white>Vg Data</color>", _title, GUILayout.Width(200));
            GUILayout.Space(10);

            var vg = _response.VirtualGoods[_index];

            var existingNames = (from v in _response.VirtualGoods where v != vg select v.ShortCode).ToArray();
            vg.ShortCode = ObjectNames.GetUniqueName(existingNames, EditorGUILayout.TextField("ShortCode", vg.ShortCode)); 
            vg.GooglePlayProductId = EditorGUILayout.TextField("GooglePlayProductId", vg.GooglePlayProductId);
            vg.IosAppStoreProductId = EditorGUILayout.TextField("IosAppStoreProductId", vg.IosAppStoreProductId);
            vg.Currency1Cost = EditorGUILayout.LongField("Coins", vg.Currency1Cost);
            vg.Currency2Cost = EditorGUILayout.LongField("Tokens", vg.Currency2Cost);
            vg.Tags = EditorGUILayout.TextField("Tags", vg.Tags);

            EditorGUILayout.BeginHorizontal();
            
            EditorGUILayout.LabelField("<color=white>Bundled Goods</color>", _title, GUILayout.Width(200));
            GUILayout.Space(10);

            if(vg.BundledShortCodes == null)
                vg.BundledShortCodes = new List<BundledGoods>();
            
            if(GUILayout.Button("+", GUILayout.Width(25)))
                vg.BundledShortCodes.Add(new BundledGoods());

            EditorGUILayout.EndHorizontal();

            for(var i=0; i<vg.BundledShortCodes.Count;i++)
                DrawBundledGood(vg.BundledShortCodes, i);

            EditorGUILayout.BeginHorizontal();
            
            EditorGUILayout.LabelField("<color=white>Segments</color>", _title, GUILayout.Width(200));
            GUILayout.Space(10);

            if (GUILayout.Button("+", GUILayout.Width(25)))
            {
                vg.SegmentData.Add(new SegmentedData());
                _foldouts.Add(false);
            }
            
            EditorGUILayout.EndHorizontal();

            if (!vg.SegmentData?.Any() ?? true)
            {
                GUILayout.Space(50);
                EditorGUILayout.EndScrollView();
                return;
            }
            
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("<color=white>Segment</color>", _subTitle);
            EditorGUILayout.LabelField("<color=white>Coins</color>", _subTitle);
            EditorGUILayout.LabelField("<color=white>Tokens</color>", _subTitle);

            EditorGUILayout.EndHorizontal();
            
            for (int i = 0; i < vg.SegmentData.Count; i++)
            {
                var segment = vg.SegmentData[i];
                
                var rect = EditorGUILayout.BeginVertical();
                
                EditorGUI.DrawRect(rect, i % 2 == 0 
                    ? new Color(0.18f, 0.18f, 0.18f) 
                    : new Color(0.13f, 0.13f, 0.13f));
                
                EditorGUILayout.BeginHorizontal();
                _foldouts[i] = EditorGUI.Foldout(new Rect(rect.position, new Vector2(15, 15)), _foldouts[i], "");
                EditorGUILayout.LabelField("", GUILayout.Width(15));
                
                if(segment.Segment == null)
                    segment.Segment = new string[1];
                
                segment.Segment[0] = EditorGUILayout.TextField(segment.Segment[0]);
                segment.Currency1Cost = EditorGUILayout.LongField(segment.Currency1Cost);
                segment.Currency2Cost = EditorGUILayout.LongField(segment.Currency2Cost);

                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    vg.SegmentData.RemoveAt(i);
                    _foldouts.RemoveAt(i);
                    i = Mathf.Clamp(i, 0, vg.SegmentData.Count - 1);
                }
                
                EditorGUILayout.EndHorizontal();

                if (!_foldouts.Any() || !_foldouts[i]) 
                    continue;

                EditorGUILayout.BeginHorizontal();
                EditorGUI.indentLevel ++;
                EditorGUILayout.LabelField("<color=white>Bundled Goods</color>", _subTitle, GUILayout.Width(200));
                
                if(GUILayout.Button("+", GUILayout.Width(25)))
                    segment.BundledShortCodes.Add(new BundledGoods());
                
                EditorGUI.indentLevel --;
                EditorGUILayout.EndHorizontal();

                if(segment.BundledShortCodes == null)
                    segment.BundledShortCodes = new List<BundledGoods>();
                
                for (int j = 0; j < segment.BundledShortCodes.Count; j++)
                    DrawBundledGood(segment.BundledShortCodes, j);
                
                EditorGUILayout.EndVertical();
            }
            
            GUILayout.Space(50);
            EditorGUILayout.EndScrollView();
        }

        void ResetFoldouts()
        {
            _foldouts = new bool[_response.VirtualGoods[_index].SegmentData.Count].ToList();
        }

        private void DrawBundledGood(List<BundledGoods> bgs, int index)
        {
            EditorGUI.indentLevel ++;

            var currentVg = _response.VirtualGoods[_index];
            var bg = bgs[index];
            var vgNames = (
                from vg in _response.VirtualGoods 
                where vg != currentVg
                select vg.ShortCode).ToArray();
            var shIndex = ArrayUtility.IndexOf(vgNames, bg.VirtualGood.ShortCode);

            if (shIndex < 0)
            {
                shIndex = 0;
                bg.VirtualGood.ShortCode = vgNames[0];
            }
            
            EditorGUILayout.BeginHorizontal();
            
            EditorGUI.BeginChangeCheck();
            shIndex = EditorGUILayout.Popup("ShortCode", shIndex, vgNames);
            if (EditorGUI.EndChangeCheck())
            {
                bg.VirtualGood.ShortCode = vgNames[shIndex];
            }

            bg.Qty = EditorGUILayout.LongField("Quantity", bg.Qty);

            if (GUILayout.Button("X", GUILayout.Width(25)))
            {
                bgs.Remove(bg);
                EditorGUILayout.EndHorizontal();
            }
                    
            EditorGUILayout.EndHorizontal();
            
            EditorGUI.indentLevel --;
        }

        private void DrawHeaderButtons()
        {
            EditorGUILayout.BeginHorizontal();

            var vg = _response.VirtualGoods[_index];

            EditorGUILayout.BeginVertical();
            
            GUI.enabled = vg.IsDirty && !string.IsNullOrEmpty(vg.ShortCode);
            
            if (GUILayout.Button("Upload Current"))
            {
                var shortCode = vg.ShortCode;

                if (vg.GetType() == typeof(NewSerializableVirtualGood))
                {
                    if (EditorUtility.DisplayDialog(
                        "Insert New Virtual Good",
                        $"The virtual good {shortCode} will be added. Are you sure?",
                        "Ok",
                        "Cancel"))
                    {
                        var vgJson = JsonUtility.ToJson(vg);
                        var request = new AddVgRequest(vgJson);
                        request.Than(() => vg.UploadCompleted());
                        AddRequests(false, null, request);
                    }
                }
                else
                {
                    if (EditorUtility.DisplayDialog(
                        "Overwrite Virtual Good",
                        $"The virtual good {shortCode} will be overwrite. Are you sure?",
                        "Ok",
                        "Cancel"))
                    {
                        var vgJson = JsonUtility.ToJson(vg);
                        var request = new SetVgRequest(shortCode, vgJson);
                        request.Than(() => vg.UploadCompleted());
                        AddRequests(false, null, request);
                    }
                }
            }
            
            GUI.enabled = _response.VirtualGoods.Any(vg1 => vg1.IsDirty);

            var dirtyCount = _response.VirtualGoods.Count(vg1 => vg1.IsDirty);
            
            if (GUILayout.Button($"Upload All ({dirtyCount})"))
            {
                if (EditorUtility.DisplayDialog(
                    "Bulk upload virtual goods",
                    $"{dirtyCount} virtual goods will be uploaded. Are you sure?",
                    "Ok",
                    "Cancel"))
                {
                    var requests = (from vg1 in _response.VirtualGoods
                        where vg1.IsDirty
                        select new SetVgRequest(vg1.ShortCode, JsonUtility.ToJson(vg1)) as
                            RestRequest).ToList();

                    AddRequests(true, () =>
                    {
                        foreach (var vg1 in _response.VirtualGoods)
                            if(vg1.IsDirty)
                                vg1.UploadCompleted();
                        
                    }, requests.ToArray());
                }
            }
            
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.BeginVertical();
            
            GUI.enabled = true;

            if (GUILayout.Button("Add New VG"))
            {
                _response.VirtualGoods.Add(new NewSerializableVirtualGood());
                _index = _response.VirtualGoods.Count - 1;
                GUI.FocusControl(null);
            }

            GUI.enabled = vg.GetType() != typeof(NewSerializableVirtualGood);
            
            if (GUILayout.Button("Delete Current"))
            {
                if(EditorUtility.DisplayDialog(
                    "Delete Virtual Good", 
                    $"The virtual good {vg.ShortCode} will be lost forever and ever. Are you sure?", 
                    "Ok", 
                    "Cancel"))
                {
                    var request = new DeleteVgRequest(vg.ShortCode);
                    request.Than(() =>
                    {
                        _response.VirtualGoods.RemoveAt(_index);
                        
                        if (_index > _response.VirtualGoods.Count - 1)
                            _index--;
                    
                        _foldouts = new bool[vg.SegmentData.Count].ToList();
                    });
                    
                    AddRequests(false, null, request);
                }
            }
            
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            
            GUI.enabled = vg.GetType() != typeof(NewSerializableVirtualGood);
            
            if (GUILayout.Button("Copy"))
            {
                _vgCopied = vg;
            }
            
            GUI.enabled = _vgCopied != null;
            
            if (GUILayout.Button("Paste"))
            {
                vg = _response.VirtualGoods[_index] = _vgCopied.Clone() as SerializableVirtualGood;
                _foldouts = new bool[vg.SegmentData.Count].ToList();
                GUI.FocusControl(null);
            }
            
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.BeginVertical();

            GUI.enabled = true;

            if (GUILayout.Button("Save To Cache"))
            {
                var text = JsonUtility.ToJson(_response);
                File.WriteAllText(VgCachePath, text);
            }

            GUI.enabled = File.Exists(VgCachePath);
            
            if (GUILayout.Button("Load From Cache"))
            {
                var text = File.ReadAllText(VgCachePath);
                _response = JsonUtility.FromJson<ListVgsResponse>(text);
                _index = 0;
            }
            
            EditorGUILayout.EndVertical();
            
            GUI.enabled = (vg.SegmentData?.Any() ?? false) && 
                          (vg.BundledShortCodes?.Any() ?? false);
            
            if (GUILayout.Button("BundledGoods To Segments"))
            {
                foreach (var segment in vg.SegmentData)
                {
                    segment.BundledShortCodes.Clear();

                    foreach (var bg in vg.BundledShortCodes)
                        segment.BundledShortCodes.Add(bg.Clone() as BundledGoods);
                }
            }
            
            GUI.enabled = true;
            
            EditorGUILayout.EndHorizontal();
        }
    }
}