using System;
using System.Linq;
using JsonEditorUtility;
using Requests.Collections;
using Requests.Collections.Data;
using UnityEditor;
using UnityEngine;

namespace Editor.Operations
{
    public class MongoCollectionOperation : BaseOperation
    {
        public override string Description => "Operation on collections";
        public override string Name => "MongoCollectionOperation";

        private CollectionsContainer _container;
        private Vector2 _scrollRect;
        private int _collectionTypeIndex, _collectionIndex, _documentIndex;
        private DocumentCollection _documents;
        private GUIStyle _titleStyle;
        private Texture2D _lockClose, _lockOpen;
        private Material _material;
        private bool _lockEditing = true;
        private JsonDrawer _jsonHandler;
        private string _fieldsQuery, _queryQuery, _sortQuery;
        private int _limitQuery, _skipQuery;
        
        public MongoCollectionOperation()
        {
            _titleStyle = new GUIStyle
            {
                richText = true,
                fontSize = 18,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleCenter
            };

            var guid = AssetDatabase.FindAssets("t:Texture2D LockClosed").FirstOrDefault();
            var path = AssetDatabase.GUIDToAssetPath(guid);
            _lockClose = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
            
            guid = AssetDatabase.FindAssets("t:Texture2D LockOpen").FirstOrDefault();
            path = AssetDatabase.GUIDToAssetPath(guid);
            _lockOpen = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
            
            _material = new Material(Shader.Find("Sprites/Default"));
        }

        public override void Initialize()
        {
            var request = new GetCollectionsRequest();
            _container = null;
            request.Than(r => _container = r);
            request.Catch(Debug.LogException);
            request.Send();
            AddRequests(false, null, request);
        }
        
        public override void DrawEditorGUI()
        {
            if (_container == null)
                return;
            
            base.DrawEditorGUI();

            EditorGUI.BeginChangeCheck();
            
            _collectionTypeIndex = EditorGUILayout.Popup("Collection Type", _collectionTypeIndex,
                Enum.GetNames(typeof(CollectionType)), GUILayout.Width(600));

            if (EditorGUI.EndChangeCheck())
            {
                _collectionIndex = 0;
                _documentIndex = 0;
                _jsonHandler = null;
            }

            var collectionType = (CollectionType) _collectionTypeIndex;
            var collectionNames = _container.CollectionNames[collectionType];
            
            EditorGUILayout.BeginHorizontal();
            
            EditorGUI.BeginChangeCheck();

            _collectionIndex = EditorGUILayout.Popup("Collection", _collectionIndex, collectionNames, GUILayout.Width(600));

            if (EditorGUI.EndChangeCheck())
            {
                _documentIndex = 0;
                _jsonHandler = null;
            }

            if (_documents != null && GUILayout.Button("↻", GUILayout.Width(25)))
            {
                _documentIndex = 0;
                _jsonHandler = null;
                FindCollection(collectionNames);
            }
            
            if (_documents != null && GUILayout.Button("←", GUILayout.Width(25)))
            {
                _documents = null;
                _documentIndex = 0;
                _jsonHandler = null;
            }
            
            EditorGUILayout.EndHorizontal();

            if (_documents == null)
            {
                GUILayout.Space(150);
                
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("", GUILayout.ExpandWidth(true));

                if (GUILayout.Button("Find", GUILayout.Width(400), GUILayout.Height(70)))
                    FindCollection(collectionNames);

                EditorGUILayout.BeginVertical();
                
                if (GUILayout.Button("Find With Query", GUILayout.Width(400), GUILayout.Height(70)))
                    FindCollectionWithQuery(collectionNames);

                _fieldsQuery = EditorGUILayout.TextField("Fields Query", _fieldsQuery, GUILayout.Width(400));
                _limitQuery = EditorGUILayout.IntField("Limit", _limitQuery, GUILayout.Width(400));
                _queryQuery = EditorGUILayout.TextField("Query", _queryQuery, GUILayout.Width(400));
                _skipQuery = EditorGUILayout.IntField("Skip", _skipQuery, GUILayout.Width(400));
                _sortQuery = EditorGUILayout.TextField("Sort Query", _sortQuery, GUILayout.Width(400));
                
                EditorGUILayout.EndVertical();
            
                EditorGUILayout.LabelField("", GUILayout.ExpandWidth(true));
            
                EditorGUILayout.EndHorizontal();
            }

            if (_documents?.Documents == null)
                return;
            
            DrawHeaderButtons(collectionNames[_collectionIndex]);

            if (!_documents.Documents.Any())
            {
                EditorGUILayout.HelpBox("This collection hasn't any document in it!", MessageType.Error);
                return;
            }

            var names = (from d in _documents.Documents select d.Id).ToList();
            DrawSearchBar(ref _documentIndex, names, () => _jsonHandler = null);

            DrawNavigationBar(ref _documentIndex, _documents.Documents, () =>
            {
                GUI.FocusControl(null);
                _jsonHandler = null;
            });
            
            GUILayout.Space(10);
            
            var document = _documents.Documents[_documentIndex];
            
            if(_jsonHandler == null)
                _jsonHandler = new JsonDrawer(document.Json);
            
            var rect = EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField($"<color=white>{document.Id}</color>", _titleStyle);
            GUILayout.Space(10);
            
            EditorGUILayout.EndVertical();

            var spriteRect = new Rect(new Vector2(rect.width - 150, rect.y - 55), new Vector2(56, 80));
            
            EditorGUI.DrawPreviewTexture(spriteRect, _lockEditing ? _lockClose : _lockOpen, _material);

            if (GUI.Button(spriteRect, "", _titleStyle))
                _lockEditing = !_lockEditing;
            
            _scrollRect = EditorGUILayout.BeginScrollView(_scrollRect);

            if (!string.IsNullOrEmpty(document.Json))
                document.Json = _jsonHandler.Draw(_lockEditing);
            
            EditorGUILayout.EndScrollView();
        }

        private void FindCollection(string[] collectionNames)
        {
            var request = new FindCollectionRequest(collectionNames[_collectionIndex]);
            request.Than(ds => _documents = ds);
            request.Catch(Debug.LogException);
            AddRequests(false, null, request);
        }
        
        private void FindCollectionWithQuery(string[] collectionNames)
        {
            var request = new FindCollectionWithQueryRequest(
                collectionNames[_collectionIndex],
                _fieldsQuery, 
                _limitQuery, 
                _queryQuery, 
                _skipQuery, 
                _sortQuery);
            
            request.Than(ds => _documents = ds);
            request.Catch(Debug.LogException);
            AddRequests(false, null, request);
        }
        
        private void DrawHeaderButtons(string currentCollectionName)
        {
            if (!_documents?.Documents?.Any() ?? true)
                return;
            
            var document = _documents.Documents[_documentIndex];
            
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Document", GUILayout.Width(150)))
            {
                var newDocument = new Document("New Document", "{}", true);
                _documents.Documents.Add(newDocument);
                _documentIndex = _documents.Documents.Count - 1;
                
                _jsonHandler = null;
                EditorGUILayout.EndHorizontal();
                return;
            }
            
            GUI.enabled = document != null;

            if (GUILayout.Button("Duplicate", GUILayout.Width(150)))
            {
                var newDocument = new Document("New Document", document.Json + "", true);
                _documents.Documents.Add(newDocument);
                _documentIndex = _documents.Documents.Count - 1;
                
                _jsonHandler = null;
                EditorGUILayout.EndHorizontal();
                return;
            }
            
            GUI.enabled = document?.IsModified ?? false;

            if (GUILayout.Button("Upload", GUILayout.Width(150)))
            {
                if (document.IsNew)
                {
                    if (EditorUtility.DisplayDialog(
                        "Insert New Document",
                        $"The document {document.Id} will be added. Are you sure?",
                        "Ok",
                        "Cancel"))
                    {
                        var request = new AddDocumentRequest(currentCollectionName, document.Json);
                        AddRequests(false, null, request);
                    }
                }
                else
                {
                    if (EditorUtility.DisplayDialog(
                        "Save Document",
                        $"The document {document.Id} will be overwrited. Are you sure?",
                        "Ok",
                        "Cancel"))
                    {
                        var request = new SaveDocumentRequest(currentCollectionName, document.Json);
                        AddRequests(false, null, request);
                    }
                }
            }
            
            if (GUILayout.Button("Restore", GUILayout.Width(150)))
            {
                //GUI disabled if document is null
                document.Json = document.OriginalJson;
            }
            
            if (GUILayout.Button("Count", GUILayout.Width(150)))
            {
                var request = new CountCollectionRequest(currentCollectionName);
                request.Send();
                request.Than(() => Debug.Log("Count Result: " + request.GetCountResult()));
                AddRequests(false, null, request);
            }

            GUI.enabled = true;
            
            EditorGUILayout.EndHorizontal();
        }
    }
}