﻿using System;
using System.Collections.Generic;
using System.Linq;
using Authentication;
using Editor.Operations;
using Requests.Base;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

public class WhatSparkEditorWindow : EditorWindow
{
    [MenuItem("WhatSparks/Main")]
    public static void WhatSparkEditorWindowShow()
    {
        var window = GetWindow<WhatSparkEditorWindow>();
        window.titleContent = new GUIContent("Main");
        window.Show();
    }

    private GUIStyle _loadingStyle, _titleStyle;
    private List<IOperation> _operations = new List<IOperation>();
    private string[] _operationNames;
    private int _opIndex;
    private AnimBool _fadeBool;

    private void OnEnable()
    {
        _operations =
            (from assembly in AppDomain.CurrentDomain.GetAssemblies()
            from t in assembly.GetTypes()
            where typeof(IOperation).IsAssignableFrom(t)
            where t != typeof(IOperation) && t != typeof(BaseOperation)
            select Activator.CreateInstance(t) as IOperation).ToList();

        _operationNames = (from op in _operations select op.Name).ToArray();

        _opIndex = 0;

        _loadingStyle = new GUIStyle
        {
            richText = true,
            alignment = TextAnchor.MiddleCenter,
            fontSize = 30,
            fontStyle = FontStyle.Bold
        };

        _titleStyle = new GUIStyle
        {
            richText = true,
            alignment = TextAnchor.MiddleCenter,
            fontSize = 24,
            fontStyle = FontStyle.Bold
        };
        
        _fadeBool = new AnimBool(true);
    }
    
    private void Update() => Repaint();
    
    private void OnGUI()
    {
        if (AuthenticationUtility.LoggedSate != LoginState.Logged)
        {
            DrawOverlayWriting("You need to log in to execute any operation!");
            return;
        }

        if (string.IsNullOrEmpty(ProjectUtility.GsApiKey))
        {
            DrawOverlayWriting("You need to have a project to execute any operation!");
            return;
        }
        
        if (!_operations.Any())
        {
            DrawOverlayWriting("No operations found!");
            return;
        }

        GUI.enabled = _fadeBool.target = _operations[_opIndex].State != RequestState.InProgress;

        EditorGUILayout.BeginHorizontal();
        
        _opIndex = EditorGUILayout.Popup("Select Operation", _opIndex, _operationNames);

        var currentOp = _operations[_opIndex];

        if (currentOp.Initialized && GUILayout.Button("↻", GUILayout.Width(25)))
        {
            currentOp.Reload();
            GUI.FocusControl(null);
        }
        
        EditorGUILayout.EndHorizontal();
        
        GUILayout.Space(15);
        EditorGUILayout.LabelField($"<color=white>{currentOp.Name}</color>", _titleStyle);
        GUILayout.Space(10);

        if(!currentOp.Initialized)
            EditorGUILayout.HelpBox(currentOp.Description, MessageType.Info);

        var rect = EditorGUILayout.BeginHorizontal();
        rect = new Rect(rect.position.x + position.width / 2 - 150, rect.position.y, 300, 35);
        
        if(!currentOp.Initialized && GUI.Button(rect, "Start"))
            currentOp.Initialize();
        
        EditorGUILayout.EndHorizontal();

        currentOp.DrawEditorGUI();

        GUI.enabled = true;
        
        if (currentOp.State == RequestState.InProgress)
            DrawLoading();
    }

    private void DrawLoading()
    {
        var loadWriting = "Loading";

        var pointCount = Mathf.RoundToInt((float) (EditorApplication.timeSinceStartup % 4));

        for (var i = 0; i < pointCount; i++)
            loadWriting += ".";

        DrawOverlayWriting(loadWriting, 1 - _fadeBool.faded);
    }

    private void DrawOverlayWriting(string message, float fade = 1)
    {
        var windowRect = new Rect(Vector2.zero, position.size);
        EditorGUI.DrawRect(windowRect, new Color(0f, 0f, 0f, 0.38f * fade));
        message = $"<color=white>{message}</color>";
        EditorGUI.LabelField(windowRect, message, _loadingStyle);
    }
}
