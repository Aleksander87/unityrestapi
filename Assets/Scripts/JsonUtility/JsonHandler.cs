using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Conditions;

namespace JsonEditorUtility
{
    /// <summary>Utility to modify and query a json string</summary>
    public class JsonHandler
    {
        private readonly Parser _parser;
        public string Json { get; private set; }

        public JsonHandler(string json)
        {
            if(string.IsNullOrEmpty(json))
                throw new ArgumentException("Json is not valid");
            
            Json = json;
            _parser = new Parser();
        }

        /// <summary>
        /// Modify a json.
        /// </summary>
        /// <param name="path">The path to the field separated by /. Ex: one/two/three</param>
        /// <param name="value">Value substituted with what is found in the json path</param>
        /// <returns>Return true if the modification is successful</returns>
        public bool Modify(string path, string value)
        {
            var result = _parser.Modify(Json, path, value);

            if (_parser.ModifySuccess)
                Json = result;
            
            return _parser.ModifySuccess;
        }

        /// <summary>
        /// Modify a json.
        /// </summary>
        /// <param name="path">The path to the field separated by /. Ex: one/two/three</param>
        /// <param name="manipulateFunc">Func that receives the value found at path, and return the new value</param>
        /// <returns>Return true if the modification is successful</returns>
        public bool Manipulate(string path, Func<string, string> manipulateFunc)
        {
            var result = _parser.Manipulate(Json, path, manipulateFunc);

            if (_parser.ModifySuccess)
                Json = result;
            
            return _parser.ModifySuccess;
        }

        /// <summary>
        /// Query a json.
        /// </summary>
        /// <param name="path">The path to the field separated by /. Ex: one/two/three</param>
        /// <param name="condition">Condition evaluated with the value found at path</param>
        /// <returns></returns>
        public bool Query(string path, ICondition condition)
        {
            return _parser.Query(Json, path, condition);
        }

        /// <summary>
        /// Find the value at a given path and returns it.
        /// </summary>
        /// <param name="path">The path to the field separated by /. Ex: one/two/three</param>
        /// <returns></returns>
        public string GetValue(string path)
        {
            return _parser.GetValue(Json, path);
        }

        private class Parser : IDisposable
        {
            private StringReader _stringReader;
            
            private bool _querySuccess, _getValue;
            private string _insertString, _valueToReturn;
            private string[] _slicedPath;
            private string _modifyValue;
            private bool _modifySuccess, _modifyNext;
            private Func<string, string> _manipulateFunc;
            private ICondition _condition;
            private int _depth;
            private bool[] _matchedPath;

            public bool ModifySuccess => _modifySuccess;

            public string Modify(string jsonString, string path, string value)
            {
                _slicedPath = path.Split('/');
                _matchedPath = new bool[_slicedPath.Length];
                
                _modifyValue = value;
                _manipulateFunc = null;
                _condition = null;
                _getValue = false;
                
                _modifySuccess = false;
                _modifyNext = false;
                _querySuccess = false;
                _depth = 0;
                
                using (_stringReader = new StringReader(jsonString))
                    return ParseValue();
            }

            public string Manipulate(string jsonString, string path, Func<string, string> manipulateFunc)
            {
                _slicedPath = path.Split('/');
                _matchedPath = new bool[_slicedPath.Length];

                _modifyValue = null;
                _manipulateFunc = manipulateFunc;
                _condition = null;
                _getValue = false;

                _modifySuccess = false;
                _modifyNext = false;
                _querySuccess = false;
                _depth = 0;

                using (_stringReader = new StringReader(jsonString))
                    return ParseValue();
            }
            
            public bool Query(string jsonString, string path, ICondition condition)
            {
                _slicedPath = path.Split('/');
                _matchedPath = new bool[_slicedPath.Length];

                _modifyValue = null;
                _manipulateFunc = null;
                _condition = condition;
                _getValue = false;

                _modifySuccess = false;
                _modifyNext = false;
                _querySuccess = false;
                _depth = 0;

                using (_stringReader = new StringReader(jsonString))
                {
                    ParseValue();
                    return _querySuccess;
                }
            }

            public string GetValue(string jsonString, string path)
            {
                _slicedPath = path.Split('/');
                _matchedPath = new bool[_slicedPath.Length];

                _modifyValue = null;
                _manipulateFunc = null;
                _condition = null;
                _getValue = true;
                
                _modifySuccess = false;
                _modifyNext = false;
                _querySuccess = false;
                _depth = 0;

                using (_stringReader = new StringReader(jsonString))
                {
                    ParseValue();
                    return _valueToReturn;
                }
            }

            public void Dispose()
            {
                _stringReader.Dispose();
                _stringReader = null;
            }

            private string ReturnCheck(string value, bool isFieldName = false)
            {
                if (!isFieldName && _modifyNext)
                {
                    _modifySuccess = true;
                    _modifyNext = false;
                    
                    if(!string.IsNullOrEmpty(_modifyValue) )
                        return _modifyValue;

                    if (_manipulateFunc != null)
                        return _manipulateFunc.Invoke(value);

                    if (_condition != null)
                        _querySuccess = _condition.Evaluate(value);

                    if (_getValue)
                        _valueToReturn = value;
                }
                
                return value;
            }

            private string ParseObject()
            {
                _depth++;
                
                var builder = new StringBuilder();
                builder.Append('{');
                _stringReader.Read();

                while (true)
                {
                    TOKEN nextToken;
                    do
                    {
                        nextToken = NextToken;
                        
                        switch (nextToken)
                        {
                            case TOKEN.NONE:
                                _depth--;
                                return ReturnCheck("");
                            case TOKEN.CURLY_OPEN:
                                builder.Append('{');
                                goto label_5;
                            case TOKEN.CURLY_CLOSE:
                                _depth--;
                                builder.Append('}');
                                return ReturnCheck(builder.ToString());
                            case TOKEN.COMMA:
                                builder.Append(ReturnCheck(","));
                                continue;
                            default:
                                continue;
                        }
                    } while (nextToken == TOKEN.COMMA);

                    
                    label_5:
                    var index = ParseString(true);

                    builder.Append(ReturnCheck(index, true));
                    
                    if (index != null)
                    {
                        if (NextToken == TOKEN.COLON)
                        {
                            _stringReader.Read();
                            var result = ReturnCheck($":{ParseValue()}");
                            builder.Append(result);
                        }
                        else
                        {
                            return ReturnCheck("");
                        }
                    }
                    else
                    {
                        return ReturnCheck("");
                    }
                }
            }

            private string ParseArray()
            {
                _stringReader.Read();
                var flag = true;
                var builder = new StringBuilder();
                var elements = new List<string>();
                
                while (flag)
                {
                    var nextToken = NextToken;
                    switch (nextToken)
                    {
                        case TOKEN.NONE:
                            break;
                        case TOKEN.SQUARED_CLOSE:
                            flag = false;
                            continue;
                        case TOKEN.COMMA:
                            continue;
                        default:
                            elements.Add(ReturnCheck(ParseByToken(nextToken)));
                            continue;
                    }
                }

                builder.Append('[');

                for (int i = 0; i < elements.Count; i++)
                {
                    builder.Append(elements[i]);

                    if (i < elements.Count - 1)
                        builder.Append(',');
                }
                
                builder.Append(']');

                return ReturnCheck(builder.ToString());
            }

            private string ParseValue()
            {
                return ParseByToken(NextToken);
            }

            private string ParseByToken(TOKEN token)
            {
                switch (token)
                {
                    case TOKEN.CURLY_OPEN:
                        return ParseObject();
                    case TOKEN.SQUARED_OPEN:
                        return ParseArray();
                    case TOKEN.STRING:
                        return ParseString(false);
                    case TOKEN.NUMBER:
                        return ParseNumber();
                    case TOKEN.TRUE:
                        return ParseBoolean(true);
                    case TOKEN.FALSE:
                        return ParseBoolean(false);
                    case TOKEN.NULL:
                        return "null";
                    default:
                        return "null";
                }
            }

            private string ParseBoolean(bool value)
            {
                return ReturnCheck(value.ToString().ToLower());
            }

            private string ParseString(bool isFieldName)
            {
                var stringBuilder1 = new StringBuilder();
                _stringReader.Read();
                var flag = true;
                while (flag)
                {
                    if (_stringReader.Peek() == -1)
                        break;
                    var nextChar1 = NextChar;
                    switch (nextChar1)
                    {
                        case '"':
                            flag = false;
                            continue;
                        case '\\':
                            if (_stringReader.Peek() == -1)
                            {
                                flag = false;
                                continue;
                            }

                            char nextChar2 = NextChar;
                            switch (nextChar2)
                            {
                                case '"':
                                case '/':
                                case '\\':
                                    stringBuilder1.Append(nextChar2);
                                    continue;
                                case 'b':
                                    stringBuilder1.Append('\b');
                                    continue;
                                case 'f':
                                    stringBuilder1.Append('\f');
                                    continue;
                                case 'n':
                                    stringBuilder1.Append('\n');
                                    continue;
                                case 'r':
                                    stringBuilder1.Append('\r');
                                    continue;
                                case 't':
                                    stringBuilder1.Append('\t');
                                    continue;
                                case 'u':
                                    StringBuilder stringBuilder2 = new StringBuilder();
                                    for (int index = 0; index < 4; ++index)
                                        stringBuilder2.Append(NextChar);
                                    stringBuilder1.Append(
                                        (char) Convert.ToInt32(stringBuilder2.ToString(), 16));
                                    continue;
                                default:
                                    continue;
                            }
                        default:
                            stringBuilder1.Append(nextChar1);
                            continue;
                    }
                }

                var result = stringBuilder1.ToString();

                if (isFieldName && _depth - 1 < _slicedPath.Length)
                {
                    var isMatching = result.Equals(_slicedPath[_depth -1]);

                    _matchedPath[_depth -1] = isMatching;
                    
                    for (var i = _depth; i < _matchedPath.Length; i++)
                        _matchedPath[i] = false;
                    
                    if (_matchedPath.All(m => m))
                        _modifyNext = true;
                }

                result = $"\"{result}\"";

                return ReturnCheck(result, isFieldName);
            }

            private string ParseNumber()
            {
                double.TryParse(NextWord, NumberStyles.Any, CultureInfo.InvariantCulture,
                    out var result);
                return ReturnCheck(result.ToString(CultureInfo.InvariantCulture));
            }

            private void EatWhitespace()
            {
                while (" \t\n\r".IndexOf(PeekChar) != -1)
                {
                    _stringReader.Read();
                    if (_stringReader.Peek() == -1)
                        break;
                }
            }

            private char PeekChar => Convert.ToChar(_stringReader.Peek());

            private char NextChar => Convert.ToChar(_stringReader.Read());

            private string NextWord
            {
                get
                {
                    var stringBuilder = new StringBuilder();
                    while (" \t\n\r{}[],:\"".IndexOf(PeekChar) == -1)
                    {
                        stringBuilder.Append(NextChar);
                        if (_stringReader.Peek() == -1)
                            break;
                    }

                    return stringBuilder.ToString();
                }
            }

            private TOKEN NextToken
            {
                get
                {
                    EatWhitespace();
                    if (_stringReader.Peek() == -1)
                        return TOKEN.NONE;
                    
                    switch (PeekChar)
                    {
                        case '"':
                            return TOKEN.STRING;
                        case ',':
                            _stringReader.Read();
                            return TOKEN.COMMA;
                        case '-':
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                        case 'I':
                        case 'N':
                            return TOKEN.NUMBER;
                        case ':':
                            return TOKEN.COLON;
                        case '[':
                            return TOKEN.SQUARED_OPEN;
                        case ']':
                            _stringReader.Read();
                            return TOKEN.SQUARED_CLOSE;
                        case '{':
                            return TOKEN.CURLY_OPEN;
                        case '}':
                            _stringReader.Read();
                            return TOKEN.CURLY_CLOSE;
                        default:
                            switch (NextWord)
                            {
                                case "false":
                                    return TOKEN.FALSE;
                                case "true":
                                    return TOKEN.TRUE;
                                case "null":
                                    return TOKEN.NULL;
                                default:
                                    return TOKEN.NONE;
                            }
                    }
                }
            }

            private enum TOKEN
            {
                NONE,
                CURLY_OPEN,
                CURLY_CLOSE,
                SQUARED_OPEN,
                SQUARED_CLOSE,
                COLON,
                COMMA,
                STRING,
                NUMBER,
                TRUE,
                FALSE,
                NULL,
            }
        }
    }
}