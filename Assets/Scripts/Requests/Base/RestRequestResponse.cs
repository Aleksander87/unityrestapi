using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Requests.Base
{
    public abstract class RestRequest<TResponse> : RestRequest
    {
        protected Action<TResponse> ResponseCallback;

        public virtual Func<string, string> ResultManipulationFunc => s => s;

        protected RestRequest(string url, string data = null) : base(url, data)
        {
            Request.downloadHandler = new DownloadHandlerBuffer();
        }

        protected override async Task SendRequest()
        {
            await base.SendRequest();

            if (!string.IsNullOrEmpty(Request.error))
                return;

            var json = Request.downloadHandler.text;

            json = ResultManipulationFunc?.Invoke(json);
            
            json = ToJsonFormat(json);

            try
            {
                var result = JsonUtility.FromJson<TResponse>(json);
                ResponseCallback?.Invoke(result);
            }
            catch (Exception e)
            {
                ErrorCallback?.Invoke(e);
            }
        }

        public virtual void Than(Action<TResponse> responseCallback) =>
            ResponseCallback += responseCallback;
        
        public static string ToJsonFormat(string gsFormat)
        {
            gsFormat = gsFormat.Replace("@ref", "_ref");
            gsFormat = gsFormat.Replace("@id", "_id");
            return gsFormat;
        }
        
        public static string ToGsFormat(string json)
        {
            json = json.Replace("_ref", "@ref");
            json = json.Replace("_id", "@id");
            return json;
        }
    }
}