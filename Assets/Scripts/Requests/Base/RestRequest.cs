using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Authentication;
using UnityEngine;
using UnityEngine.Networking;

namespace Requests.Base
{
    public abstract class RestRequest
    {
        public abstract AuthenticationType AuthType { get; }
        protected abstract WebRequestType WebRequestType { get; }

        public UnityWebRequest Request { get; }
        public virtual RequestState State { get; private set; }

        protected Action OnCompleteCallback;
        protected Action<Exception> ErrorCallback;

        private static readonly Dictionary<WebRequestType, Func<string, string, UnityWebRequest>>
            WebRequestFactoryMap = new Dictionary<WebRequestType, Func<string, string, UnityWebRequest>>
                {
                    {WebRequestType.Get, (url, data) => new UnityWebRequest(url)},
                    {WebRequestType.Delete, (url, data) => UnityWebRequest.Delete(url)},
                    {WebRequestType.Put, UnityWebRequest.Put},
                    {
                        WebRequestType.Post, (url, data) =>
                        {
                            var request = UnityWebRequest.Put(url, data);
                            request.method = "POST";
                            return request;
                        }
                    },
                    {
                        WebRequestType.Patch, (url, data) =>
                        {
                            var request = UnityWebRequest.Put(url, data);
                            request.method = "PATCH";
                            return request;
                        }
                    }
                };

        protected RestRequest(string url, string data = null)
        {
            Request = WebRequestFactoryMap[WebRequestType].Invoke(url, data);
            Request.downloadHandler = new DownloadHandlerBuffer();
            
            Debug.Log($"SEND {WebRequestType.ToString().ToUpper()}: {Request.url}");

            State = RequestState.Idle;

            //Headers
            AuthenticationUtility.SetAuthentication(this);
            Request.SetRequestHeader("Accept", "application/json");
        }

        public void Catch(Action<Exception> errorCallback) => ErrorCallback += errorCallback;
        public void Than(Action onCompleteCallback) => OnCompleteCallback += onCompleteCallback;

        public void Send() => SendRequest();

        protected virtual async Task SendRequest()
        {
            State = RequestState.InProgress;
            
            Request.SendWebRequest();

            while (!Request.isDone)
                await Task.Delay(100);

            if (string.IsNullOrEmpty(Request.error))
                OnCompleteCallback?.Invoke();
            else
                ErrorCallback?.Invoke(new Exception(Request.error));
            
            Debug.Log($"RECV: {Request.url}  -  message: {Request.downloadHandler.text}");
            
            State = RequestState.Concluded;
        }
    }

    public enum RequestState
    {
        Idle,
        InProgress,
        Concluded
    }
    
    public enum WebRequestType
    {
        Get,
        Post,
        Put,
        Delete,
        Patch
    }
}