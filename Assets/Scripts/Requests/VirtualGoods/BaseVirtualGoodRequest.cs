using Requests.Base;

namespace Requests.VirtualGoods
{
    public abstract class BaseVirtualGoodRequest : RestRequest
    {
        public static string Address =>
            $"https://config2.gamesparks.net/restv2/game/{ProjectUtility.GsApiKey}/config/~virtualGoods";

        protected BaseVirtualGoodRequest(string url, string data = null) : base(url, data)
        {
        }

        protected static string ToJsonFormat(string gsFormat)
        {
            gsFormat = gsFormat.Replace("@ref", "_ref");
            gsFormat = gsFormat.Replace("@id", "_id");
            return gsFormat;
        }
        
        protected static string ToGsFormat(string json)
        {
            json = json.Replace("_ref", "@ref");
            json = json.Replace("_id", "@id");
            return json;
        }
    }
}