using Requests.Base;

namespace Requests.VirtualGoods
{
    public class AddVgRequest : BaseVirtualGoodRequest
    {
        public override AuthenticationType AuthType => AuthenticationType.Basic;
        protected override WebRequestType WebRequestType => WebRequestType.Post;
        
        public AddVgRequest(string data) : base(Address, ToGsFormat(data))
        {
            Request.SetRequestHeader("Content-Type", "application/json");
            Send();
        }
    }
}