using System;
using UnityEngine;

namespace Requests.VirtualGoods.Data
{
    [Serializable]
    public class NewSerializableVirtualGood : SerializableVirtualGood
    {
        [SerializeField] private string _id;
        [SerializeField] private string description;
        [SerializeField] private string name;
        [SerializeField] private string type = "VGOOD";

        public override string ShortCode
        {
            get => base.ShortCode;
            set
            {
                _id = $"{VgRefer.VgPrefix}{value}";
                description = value;
                name = value;

                base.ShortCode = value;
            }
        }
    }
}