using System;
using UnityEngine;

namespace Requests.VirtualGoods.Data
{
    [Serializable]
    public class BundledGoods : ICloneable
    {
        [SerializeField] private long qty;
        [SerializeField] private VgRefer virtualGood = new VgRefer();

        public long Qty
        {
            get => qty;
            set => SetValue(ref qty, value);
        }

        public VgRefer VirtualGood
        {
            get => virtualGood;
            set => SetValue(ref virtualGood, value);
        }

        private void SetValue<T>(ref T field, T value)
        {
            if (field.Equals(value))
                return;

            field = value;
            IsDirty = true;
        }

        private bool _isDirty;

        public bool IsDirty
        {
            get
            {
                if (_isDirty)
                    return true;

                if (virtualGood.IsDirty)
                    return true;

                return false;
            }
            private set => _isDirty = value;
        }

        public object Clone()
        {
            return new BundledGoods
            {
                qty = qty,
                virtualGood = virtualGood.Clone() as VgRefer
            };
        }
    }
}