using System;
using System.Collections.Generic;
using UnityEngine;

namespace Requests.VirtualGoods.Data
{
    [Serializable]
    public class ListVgsResponse
    {
        [SerializeField] private List<SerializableVirtualGood> virtualGoods;

        public List<SerializableVirtualGood> VirtualGoods => virtualGoods;
    }
}