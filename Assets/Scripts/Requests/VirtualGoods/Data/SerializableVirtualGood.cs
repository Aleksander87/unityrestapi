using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Requests.VirtualGoods.Data
{
    [Serializable]
    public class SerializableVirtualGood : ICloneable
    {
        [SerializeField] private long currency1Cost;
        [SerializeField] private long currency2Cost;
        [SerializeField] private string googlePlayProductId;
        [SerializeField] private string iosAppStoreProductId;
        [SerializeField] private List<SegmentedData> segmentData = new List<SegmentedData>();
        [SerializeField] private string shortCode;
        [SerializeField] private string tags;
        [SerializeField] private List<BundledGoods> bundledShortCodes = new List<BundledGoods>();

        public long Currency1Cost
        {
            get => currency1Cost;
            set => SetValue(ref currency1Cost, value);
        }

        public long Currency2Cost
        {
            get => currency2Cost;
            set => SetValue(ref currency2Cost, value);
        }

        public string GooglePlayProductId
        {
            get => googlePlayProductId;
            set => SetValue(ref googlePlayProductId, value);
        }

        public string IosAppStoreProductId
        {
            get => iosAppStoreProductId;
            set => SetValue(ref iosAppStoreProductId, value);
        }

        public List<SegmentedData> SegmentData
        {
            get => segmentData;
            set => SetValue(ref segmentData, value);
        }

        public virtual string ShortCode
        {
            get => shortCode;
            set => SetValue(ref shortCode, value);
        }

        public string Tags
        {
            get => tags;
            set => SetValue(ref tags, value);
        }

        public List<BundledGoods> BundledShortCodes
        {
            get => bundledShortCodes;
            set => SetValue(ref bundledShortCodes, value);
        }

        private void SetValue<T>(ref T field, T value)
        {
            if (field?.Equals(value) ?? value == null)
                return;

            field = value;
            IsDirty = true;
        }

        private bool _isDirty;

        public bool IsDirty
        {
            get
            {
                if (_isDirty)
                    return true;

                if (segmentData?.Any(sg => sg.IsDirty) ?? false)
                    return true;
                
                if (bundledShortCodes?.Any(bs => bs.IsDirty) ?? false)
                    return true;

                return false;
            }
            private set => _isDirty = value;
        }

        public void UploadCompleted() => _isDirty = false;

        public void CopyFrom(SerializableVirtualGood other)
        {
            //     [SerializeField] private long currency1Cost;
            // [SerializeField] private long currency2Cost;
            // [SerializeField] private string googlePlayProductId;
            // [SerializeField] private string iosAppStoreProductId;
            // [SerializeField] private List<SegmentedData> segmentData = new List<SegmentedData>();
            // [SerializeField] private string shortCode;
            // [SerializeField] private string tags;
            // [SerializeField] private List<BundledGoods> bundledShortCodes = new List<BundledGoods>();

            
            
            
        }

        public virtual object Clone()
        {
            return new SerializableVirtualGood
            {
                shortCode = shortCode,
                currency1Cost = currency1Cost,
                currency2Cost = currency2Cost,
                googlePlayProductId = googlePlayProductId + "",
                iosAppStoreProductId = iosAppStoreProductId + "",
                tags = tags + "",
                segmentData = (from s in segmentData select s.Clone() as SegmentedData).ToList(),
                bundledShortCodes = (from b in bundledShortCodes select b.Clone() as BundledGoods).ToList()
            };
        }
    }
}