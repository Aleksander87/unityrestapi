using System;
using UnityEngine;

namespace Requests.VirtualGoods.Data
{
    [Serializable]
    public class VgRefer : ICloneable
    {
        public static string VgPrefix = "/~virtualGoods/";
        public bool IsDirty { get; private set; }
        
        [SerializeField] private string _ref;

        public string ShortCode
        {
            get  => string.IsNullOrEmpty(_ref) ? "" : _ref.Substring(15);
            set => SetValue(ref _ref, $"{VgPrefix}{value}");
        }
        
        private void SetValue<T>(ref T field, T value)
        {
            if (field?.Equals(value) ?? value == null)
                return;

            field = value;
            IsDirty = true;
        }

        public object Clone()
        {
            return new VgRefer
            {
                _ref = _ref
            };
        }
    }
}