using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Requests.VirtualGoods.Data
{
    [Serializable]
    public class SegmentedData : ICloneable
    {
        [SerializeField] private long currency1Cost;
        [SerializeField] private long currency2Cost;
        [SerializeField] private List<BundledGoods> bundledShortCodes = new List<BundledGoods>();
        [SerializeField] private string[] segment;

        public long Currency1Cost
        {
            get => currency1Cost;
            set => SetValue(ref currency1Cost, value);
        }

        public long Currency2Cost
        {
            get => currency2Cost;
            set => SetValue(ref currency2Cost, value);
        }

        public List<BundledGoods> BundledShortCodes
        {
            get => bundledShortCodes;
            set => SetValue(ref bundledShortCodes, value);
        }

        public string[] Segment
        {
            get => segment;
            set => SetValue(ref segment, value);
        }

        private void SetValue<T>(ref T field, T value)
        {
            if (field?.Equals(value) ?? value == null)
                return;

            field = value;
            IsDirty = true;
        }
        
        private bool _isDirty;

        public bool IsDirty
        {
            get
            {
                if (_isDirty)
                    return true;

                if (bundledShortCodes?.Any(bs => bs.IsDirty) ?? false)
                    return true;

                return false;
            }
            private set => _isDirty = value;
        }

        public object Clone()
        {
            return new SegmentedData
            {
                currency1Cost = currency1Cost,
                currency2Cost = currency2Cost,
                segment = (from s in segment select s + "").ToArray(),
                bundledShortCodes = (from b in bundledShortCodes select b.Clone() as BundledGoods).ToList()
            };
        }
    }
}