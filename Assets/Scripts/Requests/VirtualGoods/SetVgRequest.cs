using Requests.Base;
using UnityEngine;

namespace Requests.VirtualGoods
{
    public class SetVgRequest : BaseVirtualGoodRequest
    {
        public override AuthenticationType AuthType => AuthenticationType.Basic;
        protected override WebRequestType WebRequestType => WebRequestType.Patch;
        
        public SetVgRequest(string vgShortCode, string postData) 
            : base($"{Address}/{vgShortCode}", ToGsFormat(postData))
        {
            Send();
        }
    }
}