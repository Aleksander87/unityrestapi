using System;
using Requests.Base;
using Requests.VirtualGoods.Data;

namespace Requests.VirtualGoods
{
    public class GetListVgsRequest : RestRequest<ListVgsResponse>
    {
        public override AuthenticationType AuthType => AuthenticationType.Basic;
        protected override WebRequestType WebRequestType => WebRequestType.Get;

        public override Func<string, string> ResultManipulationFunc =>
            s => $"{{\"virtualGoods\":{s}}}";

        public GetListVgsRequest() : base(BaseVirtualGoodRequest.Address)
        {
        }
    }
}