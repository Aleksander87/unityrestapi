using Requests.Base;

namespace Requests.VirtualGoods
{
    public class DeleteVgRequest : BaseVirtualGoodRequest
    {
        public override AuthenticationType AuthType => AuthenticationType.Basic;
        protected override WebRequestType WebRequestType => WebRequestType.Delete;
        
        public DeleteVgRequest(string vgShortCode) : base($"{Address}/{vgShortCode}")
        {
            Send();
        }
    }
}