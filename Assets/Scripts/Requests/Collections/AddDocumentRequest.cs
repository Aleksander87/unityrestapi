using Requests.Base;

namespace Requests.Collections
{
    public class AddDocumentRequest : RestRequest
    {
        private static string Address =>
            $"https://{ProjectUtility.GsApiKey.ToLower()}.{ProjectUtility.Ambient}.cluster.gamesparks.net" +
            $"/restv2/game/{ProjectUtility.GsApiKey}/mongo/collection/{{0}}/insert";
        
        public AddDocumentRequest(string collectionName, string data) 
            : base(string.Format(Address, collectionName), data)
        {
            Request.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
            Send();
        }

        public override AuthenticationType AuthType => AuthenticationType.JsonWebToken;
        protected override WebRequestType WebRequestType => WebRequestType.Post;
    }
}