using System;
using Requests.Base;
using UnityEngine;

namespace Requests.Collections
{
    public class CountCollectionRequest : RestRequest
    {
        private static string Address =>
            $"https://{ProjectUtility.GsApiKey.ToLower()}.{ProjectUtility.Ambient}.cluster.gamesparks.net" +
            $"/restv2/game/{ProjectUtility.GsApiKey}/mongo/collection/{{0}}/count";

        public override AuthenticationType AuthType => AuthenticationType.JsonWebToken;
        protected override WebRequestType WebRequestType => WebRequestType.Post;
        
        private CountResult _countResult;
        
        public CountCollectionRequest(string collectionName) 
            : base(string.Format(Address, collectionName), @"{""query"": {}}")
        {
            Request.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
        }

        public long GetCountResult()
        {
            if (_countResult != null)
                return _countResult.count;
            
            var json = Request.downloadHandler.text;
            _countResult = JsonUtility.FromJson<CountResult>(json);
            
            return _countResult.count;
        }

        [Serializable]
        public class CountResult
        {
            public int count;
        }
    }
}