namespace Requests.Collections
{
    public class RemoveDocumentRequest : RemoveDocumentsByQueryRequest
    {
        public RemoveDocumentRequest(string collectionName, string documentId) 
            : base(collectionName, $"{{\"_id\":\"{documentId}\"}}")
        {
        }
    }
}