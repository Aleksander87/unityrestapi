using UnityEngine;

namespace Requests.Collections
{
    public class FindCollectionWithQueryRequest : FindCollectionRequest
    {
        public FindCollectionWithQueryRequest(string collectionName, string query) 
            : base(collectionName, CreateQuery(null, 0, query, 0, null))
        {
        }
        
        public FindCollectionWithQueryRequest(
            string collectionName, 
            string fieldsQuery,
            int limit,
            string query,
            int skip,
            string sort) 
            : base(collectionName, CreateQuery(fieldsQuery, limit, query, skip, sort))
        {
            Debug.Log(CreateQuery(fieldsQuery, limit, query, skip, sort));
        }

        private static string CreateQuery(string fieldsQuery,
            int limit,
            string query,
            int skip,
            string sort)
        {
            return "{" +
                   $"\"fields\":{{{fieldsQuery}}}," +
                   $"\"limit\":{Mathf.Max(0, limit)}," +
                   $"\"query\":{{{query}}}," +
                   $"\"skip\":{Mathf.Max(0, skip)}," +
                   $"\"sort\":{{{sort}}}" +
                   "}";
        }
    }
}