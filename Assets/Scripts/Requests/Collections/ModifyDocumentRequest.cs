using System;
using System.Linq;
using System.Threading.Tasks;
using JsonEditorUtility;
using Requests.Base;
using UnityEngine;

namespace Requests.Collections.Data
{
    public class ModifyDocumentRequest : FindCollectionRequest
    {
        private DocumentCollection _documents;
        private readonly Func<string, string> _manipulationFunc;
        private readonly string _path, _documentName, _collectionName;
        private SaveDocumentRequest _saveRequest;

        public override RequestState State => _saveRequest?.State ?? base.State;

        public ModifyDocumentRequest(
            string collectionName, 
            string documentName,
            string path,
            Func<string, string> manipulationFunc) 
            : base(collectionName)
        {
            _collectionName = collectionName;
            _documentName = documentName;
            _manipulationFunc = manipulationFunc;
            _path = path;
        }

        protected override async Task SendRequest()
        {
            Than(docs => _documents = docs);
            
            await base.SendRequest();
            
            while (_documents == null)
                await Task.Delay(100);
            
            var document = _documents.Documents.First(d => d.Id == _documentName);
            
            var jsonHandler = new JsonHandler(document.Json);
            
            if (jsonHandler.Manipulate(_path, _manipulationFunc))
                _saveRequest = new SaveDocumentRequest(_collectionName, jsonHandler.Json);
            else
                throw new InvalidOperationException();
        }
    }
}