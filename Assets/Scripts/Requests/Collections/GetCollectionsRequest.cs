using System;
using Requests.Base;
using Requests.Collections.Data;

namespace Requests.Collections
{
    public class GetCollectionsRequest : RestRequest<CollectionsContainer>
    {
        private static string Address =>
            $"https://{ProjectUtility.GsApiKey.ToLower()}.{ProjectUtility.Ambient}.cluster.gamesparks.net" +
            $"/restv2/game/{ProjectUtility.GsApiKey}/mongo/collections";
        
        public override AuthenticationType AuthType => AuthenticationType.JsonWebToken;
        protected override WebRequestType WebRequestType => WebRequestType.Get;
        
        public override Func<string, string> ResultManipulationFunc =>
            s => $"{{\"collections\":{s}}}";
        
        public GetCollectionsRequest() : base(Address)
        {
        }
    }
}