using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Requests.Base;
using Requests.Collections.Data;
using UnityEngine;

namespace Requests.Collections
{
    public class FindCollectionRequest : RestRequest<DocumentCollection>
    {
        private static string Address =>
            $"https://{ProjectUtility.GsApiKey.ToLower()}.{ProjectUtility.Ambient}.cluster.gamesparks.net" +
            $"/restv2/game/{ProjectUtility.GsApiKey}/mongo/collection/";

        public override Func<string, string> ResultManipulationFunc =>
            s => $"{{\"documents\":{s}}}";

        private Action<DocumentCollection> _responseCallback;
        private DocumentCollection _documentCollection;

        public FindCollectionRequest(string collectionName, string query = "{}") 
            : base($"{Address}{collectionName}/find", string.IsNullOrEmpty(query) ? "{}" : query)
        {
            Request.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
            Send();
        }

        protected override async Task SendRequest()
        {
            try
            {
                _documentCollection = null;
                base.Than(r => _documentCollection = r);

                await base.SendRequest();

                while (_documentCollection == null)
                    await Task.Delay(100);

                if ((_documentCollection?.Documents?.Count ?? 0) == 0)
                {
                    _responseCallback?.Invoke(new DocumentCollection(new List<Document>()));
                    return;
                }

                var documentsJson = Request.downloadHandler.text;

                var documents = new List<string>();

                StringReader stringReader;

                using (stringReader = new StringReader(documentsJson))
                {
                    var concluded = false;

                    while (true)
                    {
                        var documentBuilder = new StringBuilder();
                        var openCloseCounter = 0;

                        while (true)
                        {
                            var peek = stringReader.Peek();

                            if (peek == -1)
                            {
                                concluded = true;
                                break;
                            }

                            var nextChar = (char) peek;

                            if (nextChar == '{')
                                break;
                        
                            stringReader.Read();
                        }

                        if (concluded)
                            break;

                        do
                        {
                            var nextChar = (char)stringReader.Read();
                        
                            if (nextChar == '{')
                                openCloseCounter++;
                            else if(nextChar == '}')
                                openCloseCounter--;

                            documentBuilder.Append(nextChar);
                        
                        } while (openCloseCounter != 0);
                    
                        documents.Add(documentBuilder.ToString());
                    }
                }

                var documentList = new List<Document>();

                for (int i = 0; i < documents.Count; i++)
                    documentList.Add(new Document(_documentCollection.Documents[i].Id, documents[i]));

                _responseCallback?.Invoke(_documentCollection = new DocumentCollection(documentList));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public List<T> ConvertTo<T>()
        {
            var list = new List<T>();

            foreach (var doc in _documentCollection.Documents)
            {
                var d = JsonUtility.FromJson<T>(doc.Json);
                list.Add(d);
            }

            return list;
        }

        public override void Than(Action<DocumentCollection> responseCallback)
        {
            _responseCallback += responseCallback;
        }

        public override AuthenticationType AuthType => AuthenticationType.JsonWebToken;
        protected override WebRequestType WebRequestType => WebRequestType.Post;
    }
}