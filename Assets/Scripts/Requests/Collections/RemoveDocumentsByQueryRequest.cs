using Requests.Base;

namespace Requests.Collections
{
    public class RemoveDocumentsByQueryRequest : RestRequest
    {
        private static string Address =>
            $"https://{ProjectUtility.GsApiKey.ToLower()}.{ProjectUtility.Ambient}.cluster.gamesparks.net" +
            $"/restv2/game/{ProjectUtility.GsApiKey}/mongo/collection/{{0}}/remove";
        
        public RemoveDocumentsByQueryRequest(string collectionName, string query) 
            : base(string.Format(Address, collectionName), $"{{\"query\":{query}}}")
        {
            Request.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
        }

        public override AuthenticationType AuthType => AuthenticationType.JsonWebToken;
        protected override WebRequestType WebRequestType => WebRequestType.Post;
    }
}