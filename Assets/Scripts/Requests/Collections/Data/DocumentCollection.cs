using System;
using System.Collections.Generic;
using UnityEngine;

namespace Requests.Collections.Data
{
    [Serializable]
    public class DocumentCollection
    {
        [SerializeField] private List<Document> documents;

        public List<Document> Documents => documents;

        public DocumentCollection(List<Document> documents)
        {
            this.documents = documents;
        }
    }
}