using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Requests.Collections.Data
{
    [Serializable]
    public class CollectionsContainer
    {
        [SerializeField] private List<MongoCollection> collections;

        private CollectionIndexer _collectionIndexer;
        private CollectionNameIndexer _collectionNameIndexer;

        public CollectionIndexer Collection => _collectionIndexer ?? 
                                               (_collectionIndexer = new CollectionIndexer(collections));

        public CollectionNameIndexer CollectionNames => _collectionNameIndexer ??
                                                        (_collectionNameIndexer = new CollectionNameIndexer(collections));

        public class CollectionIndexer
        {
            private List<MongoCollection> _allCollections, _metaCollections, _runTimeCollections, _otherCollections;

            public CollectionIndexer(List<MongoCollection> allCollections)
            {
                _allCollections = allCollections;
            }
            
            public List<MongoCollection> this[CollectionType collectionType] 
            {
                get
                {
                    switch (collectionType)
                    {
                        case CollectionType.Meta:
                            return MetaCollections;
                        case CollectionType.RunTime:
                            return RunTimeCollections;
                        case CollectionType.Other:
                            return OtherCollections;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(collectionType), collectionType, null);
                    }
                }
            }
            
            private List<MongoCollection> MetaCollections => _metaCollections ?? (_metaCollections =
                _allCollections.Where(c => c.CollectionType == MongoCollectionType.Metadata).ToList());
        
            private List<MongoCollection> RunTimeCollections => _runTimeCollections ?? (_runTimeCollections =
                _allCollections.Where(c => c.CollectionType == MongoCollectionType.Runtime).ToList());
            
            private List<MongoCollection> OtherCollections => _otherCollections ?? (_otherCollections =
                _allCollections.Where(c => c.CollectionType != MongoCollectionType.Runtime && 
                                           c.CollectionType != MongoCollectionType.Metadata).ToList());
        }
        
        public class CollectionNameIndexer
        {
            private List<MongoCollection> _allCollections;
            private string[] _metaCollectionNames, _runTimeCollectionNames, _otherCollectionNames;

            public CollectionNameIndexer(List<MongoCollection> allCollections)
            {
                _allCollections = allCollections;
            }

            public string[] this[CollectionType collectionType]
            {
                get
                {
                    switch (collectionType)
                    {
                        case CollectionType.Meta:
                            return MetaCollectionNames;
                        case CollectionType.RunTime:
                            return RunTimeCollectionNames;
                        case CollectionType.Other:
                            return OtherCollectionNames;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(collectionType), collectionType, null);
                    }
                }
            }
            
            private string[] MetaCollectionNames => _metaCollectionNames ?? (_metaCollectionNames =
                _allCollections.Where(c => c.CollectionType == MongoCollectionType.Metadata).Select(c => c.Name).ToArray());
        
            private string[] RunTimeCollectionNames => _runTimeCollectionNames ?? (_runTimeCollectionNames =
                _allCollections.Where(c => c.CollectionType == MongoCollectionType.Runtime).Select(c => c.Name).ToArray());
            
            private string[] OtherCollectionNames => _otherCollectionNames ?? (_otherCollectionNames =
                _allCollections.Where(c => c.CollectionType != MongoCollectionType.Runtime && 
                                           c.CollectionType != MongoCollectionType.Metadata).Select(c => c.Name).ToArray());
        }
    }
    
    public enum CollectionType
    {
        Meta,
        RunTime,
        Other
    }
}