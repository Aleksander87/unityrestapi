using System;
using UnityEngine;

namespace Requests.Collections.Data
{
    [Serializable]
    public class MongoCollection
    {
        [SerializeField] private string name, optionGroup;

        public string Name => name;

        public MongoCollectionType CollectionType =>
            Enum.TryParse(optionGroup, true, out MongoCollectionType result)
                ? result
                : MongoCollectionType.Other;
    }

    public enum MongoCollectionType
    {
        Other,
        Metadata,
        Runtime
    }
}