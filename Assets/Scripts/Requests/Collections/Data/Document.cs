using System;
using UnityEngine;

namespace Requests.Collections.Data
{
    [Serializable]
    public class Document
    {
        [SerializeField] private string _id;

        public string Id => _id;
        public string Json { get; set; }
        public string OriginalJson { get; }
        public bool IsNew { get; }
        public bool IsModified => !OriginalJson.Equals(Json) || IsNew;
        

        public Document(string id, string json, bool isNew = false)
        {
            _id = id;
            Json = json;
            OriginalJson = json + "";
            IsNew = isNew;
        }
    }
}