using System;
using UnityEngine;

namespace Conditions
{
    [Serializable]
    public abstract class BaseCondition<T> : ICondition
    {
        [SerializeField] protected bool equals;
        [SerializeField] protected T valueToCheckAgainst;

        protected BaseCondition(bool equals, T valueToCheckAgainst)
        {
            this.equals = equals;
            this.valueToCheckAgainst = valueToCheckAgainst;
        }

        public virtual bool Evaluate(string value)
        {
            Debug.Log($"Checking {value} with {valueToCheckAgainst}");
            
            return equals
                ? value.Equals(valueToCheckAgainst.ToString())
                : !value.Equals(valueToCheckAgainst.ToString());
        }
    }

    public interface ICondition
    {
        bool Evaluate(string value);
    }
}