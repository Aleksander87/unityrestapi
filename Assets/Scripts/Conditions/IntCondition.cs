using System;
using UnityEngine;

namespace Conditions
{
    [Serializable]
    public class IntCondition : BaseCondition<int>
    {
        [SerializeField] private bool isLessThan, isMoreThan;

        public override bool Evaluate(string value)
        {
            if(!int.TryParse(value, out var result))
                throw new InvalidOperationException($"Value: {value}, can't be parsed to int");
            
            if (isLessThan)
                return result < valueToCheckAgainst;
            if (isMoreThan)
                return result > valueToCheckAgainst;
            return base.Evaluate(value);
        }

        public IntCondition(bool equals, int valueToCheckAgainst) : base(equals, valueToCheckAgainst)
        {
        }
    }
}