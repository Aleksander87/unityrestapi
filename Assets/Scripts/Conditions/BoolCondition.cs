using System;

namespace Conditions
{
    [Serializable]
    public class BoolCondition : BaseCondition<bool> {
        
        public BoolCondition(bool equals, bool valueToCheckAgainst) : base(equals, valueToCheckAgainst)
        {
        }

        public override bool Evaluate(string value)
        {
            value = bool.TryParse(value, out var result) ? result.ToString() : value;
            
            return base.Evaluate(value);
        }
    }
}