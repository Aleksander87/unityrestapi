using System;
using UnityEngine;

namespace Conditions
{
    [Serializable]
    public class StringCondition : BaseCondition<string>
    {
        [SerializeField] private bool caseSensitive;
        
        public StringCondition(bool equals, string valueToCheckAgainst, bool caseSensitive = false) 
            : base(equals, valueToCheckAgainst)
        {
            this.caseSensitive = caseSensitive;
        }
        
        public override bool Evaluate(string value)
        {
            var v = value.Substring(1, value.Length - 2);

            if (!caseSensitive)
            {
                v = v.ToLower();
                valueToCheckAgainst = valueToCheckAgainst.ToLower();
            }
            
            return base.Evaluate(v);
        }
    }
}