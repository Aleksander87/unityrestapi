using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.IO;

public static class ProjectUtility
{
    public static readonly string CachedGameAddress = $"{Application.dataPath}/Prefs/[ProjectUtility] Games";
    public static string GsApiKey;
    public static GsAmbient Ambient;

    public static void AddGame(string name, string apiKey)
    {
        var game = new Game(name, apiKey);
        var games = LoadGames();
        games.Add(game);
        SaveGames(games);
        
        Debug.Log($"Added game project: {name}, api key: {apiKey} with success");
    }

    public static void RemoveGame(string name)
    {
        var games = LoadGames();
        var game = games.FirstOrDefault(g => g.Name == name);
        games.Remove(game);
        SaveGames(games);
    }

    public static List<Game> GetGames()
    {
        return LoadGames();
    }

    private static List<Game> LoadGames()
    {
        if (!File.Exists(CachedGameAddress)) 
            return new List<Game>();

        var json = File.ReadAllText(CachedGameAddress);
        var games = JsonUtility.FromJson<GameContainer>(json).Games;
        return games;
    }

    private static void SaveGames(List<Game> games)
    {
        var container = new GameContainer(games);
        var json = JsonUtility.ToJson(container);
        File.WriteAllText(CachedGameAddress, json);
    }

    public enum GsAmbient
    {
        preview,
        live
    }
}

[Serializable]
public class GameContainer
{
    [SerializeField] private List<Game> games;

    public List<Game> Games => games;

    public GameContainer(List<Game> games)
    {
        this.games = games;
    }
}

[Serializable]
public class Game
{
    [SerializeField] private  string name, apiKey;

    public string Name => name;
    public string ApiKey => apiKey;

    public Game(string name, string apiKey)
    {
        this.name = name;
        this.apiKey = apiKey;
    }
}