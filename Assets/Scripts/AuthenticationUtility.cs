using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Requests.Base;
using UnityEngine;
using UnityEngine.Networking;

namespace Authentication
{
    public static class AuthenticationUtility
    {
        public static readonly string UserBasicAddress = $"{Application.dataPath}/Prefs/[AuthenticationUtility] Basic";
        public static LoginState LoggedSate { get; private set; }
        private static readonly Dictionary<AuthenticationType, HeaderData> Auths = 
            new Dictionary<AuthenticationType, HeaderData>();
        public static string UserName { get; private set; }

        private static UnityWebRequest _accessTokenRequest;

        public static void SetAuthentication(RestRequest restRequest)
        {
            var auth = Auths[restRequest.AuthType];
            restRequest.Request.SetRequestHeader(auth.Name, auth.Value);
        }

        public static void Login(string username, string password, bool remember = false)
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes($"{username}:{password}");
            var basicAuth = $"Basic {Convert.ToBase64String(bytes)}";

            if (remember)
                File.WriteAllText(UserBasicAddress, basicAuth);
            
            CreateAuth(basicAuth);
        }

        public static void AutoLogin()
        {
            if (!File.Exists(UserBasicAddress))
                return;
            
            CreateAuth(File.ReadAllText(UserBasicAddress));
        }

        public static void Logout()
        {
            LoggedSate = LoginState.NotLogged;
            UserName = "";
            Auths.Clear();
        }

        private static void CreateAuth(string basic)
        {
            Auths.Clear();
            Auths.Add(AuthenticationType.Basic, new HeaderData("Authorization", basic));

            _accessTokenRequest = new UnityWebRequest
            {
                url = "https://auth.gamesparks.net/restv2/auth/user",
                downloadHandler = new DownloadHandlerBuffer()
            };
            
            _accessTokenRequest.SetRequestHeader(
                Auths[AuthenticationType.Basic].Name, 
                Auths[AuthenticationType.Basic].Value);
        
            var jsonTokenRequest = new UnityWebRequest
            {
                url = $"https://auth.gamesparks.net/restv2/auth/game/{ProjectUtility.GsApiKey}/jwt",
                downloadHandler = new DownloadHandlerBuffer()
            };
            
            jsonTokenRequest.SetRequestHeader(
                Auths[AuthenticationType.Basic].Name, 
                Auths[AuthenticationType.Basic].Value);

            LoggedSate = LoginState.LogInProgress;
            
            Debug.Log("SEND - Login");

            WaitResponse(_accessTokenRequest, s =>
            {
                if (string.IsNullOrEmpty(s))
                {
                    LoggedSate = LoginState.NotLogged;
                    throw new Exception("Login Failed");
                }
                
                Debug.Log($"RECV - Login: AccessToken {s}");

                LoginResponse response = null;
                
                try
                {
                    response = JsonUtility.FromJson<LoginResponse>(s);
                }
                catch (Exception e)
                {
                    LoggedSate = LoginState.NotLogged;
                    throw new Exception("Login Failed");
                }
                
                UserName = response.Username;
                
                if(string.IsNullOrEmpty(UserName))
                {
                    LoggedSate = LoginState.NotLogged;
                    throw new Exception("Login Failed");
                }

                s = s.Substring(s.IndexOf("X-GSAccessToken\":\"") + 18);
                var end = s.IndexOf('"');
                s = s.Substring(0, end);
                Auths.Add(AuthenticationType.AccessToken, new HeaderData("X-GSAccessToken", s));
                
                LoggedSate = Auths.ContainsKey(AuthenticationType.JsonWebToken) 
                    ? LoginState.Logged
                    : LoginState.LogInProgress;
            });
        
            WaitResponse(jsonTokenRequest, s =>
            {
                if (string.IsNullOrEmpty(s))
                {
                    LoggedSate = LoginState.NotLogged;
                    throw new Exception("Login Failed");
                }
                
                Debug.Log($"RECV - Login: JsonWebToken {s}");
                
                LoginResponse response = null;
                
                try
                {
                    response = JsonUtility.FromJson<LoginResponse>(s);
                }
                catch (Exception e)
                {
                    LoggedSate = LoginState.NotLogged;
                    throw new Exception("Login Failed");
                }
                
                UserName = response.Username;
                
                if(string.IsNullOrEmpty(UserName))
                {
                    LoggedSate = LoginState.NotLogged;
                    throw new Exception("Login Failed");
                }
                
                s = s.Substring(s.IndexOf("X-GS-JWT\":\"") + 11);
                var end = s.IndexOf('"');
                s = s.Substring(0, end);
                Auths.Add(AuthenticationType.JsonWebToken, new HeaderData("X-GS-JWT", s));
                
                LoggedSate = Auths.ContainsKey(AuthenticationType.AccessToken) 
                    ? LoginState.Logged
                    : LoginState.LogInProgress;
            });
        }

        public static void CancelLogin()
        {
            if (_accessTokenRequest == null)
                return;
            
            _accessTokenRequest.Abort();
            LoggedSate = LoginState.NotLogged;
        }
        
        private static async void WaitResponse(UnityWebRequest request, Action<string> onComplete)
        {
            request.SendWebRequest();

            while (!request.isDone)
                await Task.Delay(100);
        
            onComplete?.Invoke(request.downloadHandler.text);
        }
        
        private struct HeaderData
        {
            public string Name { get; }
            public string Value { get; }

            public HeaderData(string name, string value)
            {
                Name = name;
                Value = value;
            }
        }
    }

    [Serializable]
    public class LoginResponse
    {
        [SerializeField] private string username;
        public string Username => username;
    }
}

public enum AuthenticationType
{
    Basic,
    AccessToken,
    JsonWebToken
}

public enum LoginState
{
    NotLogged,
    Logged,
    LogInProgress
}